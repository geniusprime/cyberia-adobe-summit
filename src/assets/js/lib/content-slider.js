import $ from 'jquery'
import 'slick-carousel';
const contentSlider = () => {

  $(document).ready(function () {

    var clcSliderIsAnimating = false;
    var activeContent =  $('.content-slider-topics a.clc.active');
    if (activeContent.length > 0) {
      var curEl = activeContent.attr('class').replace('active', '').trim();
      $('.page-indicator-cur').text(curEl.match(/\d+/)[0]);
      $('.content-slider-content .' + curEl).slideDown().addClass('active');
    }

    var totalEl = $('.content-slider-topics.large a.clc').length;

    if (totalEl < 10) totalEl = '0' + totalEl;

    $('.content-slider-topics .page-indicator-max').text(totalEl);

    // add slick slider
    // $('.clc-slider').slick({
    //   arrows: false
    // });

    var mobileCounter = 1;
    var totalElMobile = $('.hide-for-large .content-slider-topics a.clc').length;

    $('.clc-slider,section#community .content-slider').on('swipe', function (event, slick, direction) {
      //var tmpPage = $('.page-indicator-cur').text().match(/\d+/)[0];
      if (direction == 'left') {
        // next

        curEl = $('.content-slider-topics a.clc.active').removeClass('active');

        if(mobileCounter < totalElMobile)
        {
          curEl.parent().next().find("a.clc").addClass('active');
          curEl.next().addClass('active');
          mobileCounter += 1;
        }else{
          $('.content-slider-topics a.clc01').addClass('active');
          mobileCounter = 1;
        }

      } else if (direction == 'right') {
        // prev
        curEl = $('.content-slider-topics a.clc.active').removeClass('active');

        if(mobileCounter <= totalElMobile && mobileCounter !== 1)
        {
          curEl.parent().prev().find("a.clc").addClass('active');
          curEl.prev().addClass('active');
          mobileCounter -= 1;
        }else{
          $('.content-slider-topics a.clc07').addClass('active');
          mobileCounter = totalElMobile;
        }
      }
    });

    $('.clc-slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
      clcSliderIsAnimating = true;
    });
    $('.clc-slider').on('afterChange', function (event, slick, currentSlide) {
      clcSliderIsAnimating = false;
    });

    $('.content-slider-topics a.clc,section#community a.clc').click(function (e) {
      e.preventDefault();

      if (!$(this).hasClass('active') && !clcSliderIsAnimating) {
        var selectedEl = $(this).attr('class').replace('active', '').trim();
        
        $('.content-slider-topics a.clc.active,section#community a.clc.active').removeClass('active');
        $(this).addClass('active');

        var goToSlide = selectedEl.match(/\d+/) - 1;

        $('.clc-slider,.content-slider').slick('slickGoTo', goToSlide);

        /*
        $('.content-slider-content .' + curEl ).stop().slideUp().removeClass('active');
        $('.content-slider-content .' + selectedEl).stop().slideDown().addClass('active');
        */

        var curIndicator = selectedEl.match(/\d+/)[0];
        $('.page-indicator-cur').text(curIndicator);

        curEl = selectedEl;
      }
    });

    $('a.next').click(function (e) {
      e.preventDefault();
     

      if(mobileCounter < totalElMobile)
      {
        curEl = $('.content-slider-topics a.clc.active').removeClass('active');
        curEl.parent().next().find("a.clc").addClass('active');
        curEl.next().addClass('active');
        $('.clc-slider,section#community .content-slider').slick('slickNext');
        mobileCounter += 1;
      }else{
        return false;
      }

      if ($('.content-slider-topics a.clc.active').next().hasClass("clc") && !clcSliderIsAnimating) {
        var nextEl = $('.content-slider-topics a.clc.active').next();
        var selectedEl = $(nextEl).attr('class').replace('active', '').trim();
        $('.content-slider-topics a.clc.active').removeClass('active');
        $(nextEl).addClass('active');

        $('.clc-slider').slick('slickNext');
        /*
        $('.content-slider-content .' + curEl ).stop().slideUp().removeClass('active');
        $('.content-slider-content .' + selectedEl).stop().slideDown().addClass('active');
        */

        var curIndicator = selectedEl.match(/\d+/)[0];
        $('.page-indicator-cur').text(curIndicator);

        curEl = selectedEl;
      }
    });

    $('a.prev').click(function (e) {
      e.preventDefault();

      

      if(mobileCounter <= totalElMobile && mobileCounter !== 1)
      {
        curEl = $('.content-slider-topics a.clc.active').removeClass('active');
        curEl.parent().prev().find("a.clc").addClass('active');
        curEl.prev().addClass('active');
        $('.clc-slider,section#community .content-slider').slick('slickPrev');
        mobileCounter -= 1;
      }else{
        return false;
      }

      if ($('.content-slider-topics a.clc.active').prev().length && !clcSliderIsAnimating) {
        var prevEl = $('.content-slider-topics a.clc.active').prev();
        var selectedEl = $(prevEl).attr('class').replace('active', '').trim();
        $('.content-slider-topics a.clc.active').removeClass('active');
        $(prevEl).addClass('active');

        $('.clc-slider,section#community .content-slider').slick('slickPrev');

        /*
        $('.content-slider-content .' + curEl ).stop().slideUp().removeClass('active');
        $('.content-slider-content .' + selectedEl).stop().slideDown().addClass('active');
        */

        var curIndicator = selectedEl.match(/\d+/)[0];
        $('.page-indicator-cur').text(curIndicator);

        curEl = selectedEl;
      }
    });

    $('.clc-slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
      $("ul.accordion").foundation("up",$(".accordion-content"));
    });

  });
}

export default contentSlider;