import $ from 'jquery';
'use strict';

$(document).ready(function () {

	$('.experiences-slider').slick({
		slidesToShow: 1,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 5000
	});

	$('.reasons-slider').slick({
		slidesToShow: 1.01,
		initialSlide: 1,
		infinite: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 5000
	});

	$('.content-slider').slick({
		slidesToShow: 1,
		initialSlide: 0,
		infinite: false,
		arrows: false,
		dots: false,
		autoplay: false,
		autoplaySpeed: 5000
	});

	$('.content-slider-mobile.hide-for-large,.content-slider .hide-for-medium').slick({
		slidesToShow: 1,
		arrows: false,
		dots: true
	});

	var curMobile = $('.content-slider-mobile').slick('slickCurrentSlide') + 1;
	var maxMobile = $('.content-slider-mobile .slick-dots li').length;

	if (curMobile < 10) curMobile = '0' + curMobile;
	if (maxMobile < 10) maxMobile = '0' + maxMobile;

	$('.content-slider-controls .page-indicator-cur').text(curMobile);
	$('.content-slider-controls .page-indicator-max').text(maxMobile);

	$('.content-slider-mobile').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
		// update counter
		var slideCount = nextSlide + 1;
		if (slideCount < 10) slideCount = '0' + slideCount;
		$('.content-slider-controls .page-indicator-cur').text(slideCount);
	});

	$('.content-slider-controls .page-arrows a.prev').click(function (e) {
		e.preventDefault();
		$('.content-slider-mobile').slick('slickPrev');
	});
	$('.content-slider-controls .page-arrows a.next').click(function (e) {
		e.preventDefault();
		$('.content-slider-mobile').slick('slickNext');
	});
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNsaWNrLmpzIl0sIm5hbWVzIjpbIiQiLCJkb2N1bWVudCIsInJlYWR5Iiwic2xpY2siLCJzbGlkZXNUb1Nob3ciLCJhcnJvd3MiLCJhdXRvcGxheSIsImF1dG9wbGF5U3BlZWQiLCJpbml0aWFsU2xpZGUiLCJpbmZpbml0ZSIsImRvdHMiLCJjdXJNb2JpbGUiLCJtYXhNb2JpbGUiLCJsZW5ndGgiLCJ0ZXh0Iiwib24iLCJldmVudCIsImN1cnJlbnRTbGlkZSIsIm5leHRTbGlkZSIsInNsaWRlQ291bnQiLCJjbGljayIsImUiLCJwcmV2ZW50RGVmYXVsdCJdLCJtYXBwaW5ncyI6Ijs7QUFBQUEsRUFBRUMsUUFBRixFQUFZQyxLQUFaLENBQWtCLFlBQVc7O0FBRTVCRixHQUFFLHFCQUFGLEVBQXlCRyxLQUF6QixDQUErQjtBQUM5QkMsZ0JBQWMsQ0FEZ0I7QUFFOUJDLFVBQVEsS0FGc0I7QUFHOUJDLFlBQVUsSUFIb0I7QUFJOUJDLGlCQUFlO0FBSmUsRUFBL0I7O0FBT0NQLEdBQUUsaUJBQUYsRUFBcUJHLEtBQXJCLENBQTJCO0FBQ3pCQyxnQkFBYyxJQURXO0FBRXpCSSxnQkFBYyxDQUZXO0FBR3pCQyxZQUFVLElBSGU7QUFJekJKLFVBQVEsS0FKaUI7QUFLekJDLFlBQVUsSUFMZTtBQU16QkMsaUJBQWU7QUFOVSxFQUEzQjs7QUFTQVAsR0FBRSxpQkFBRixFQUFxQkcsS0FBckIsQ0FBMkI7QUFDekJDLGdCQUFjLElBRFc7QUFFekJJLGdCQUFjLENBRlc7QUFHekJDLFlBQVUsSUFIZTtBQUl6QkosVUFBUSxLQUppQjtBQUt6QjtBQUNBRSxpQkFBZTtBQU5VLEVBQTNCOztBQVNEUCxHQUFFLHdCQUFGLEVBQTRCRyxLQUE1QixDQUFrQztBQUNqQ0MsZ0JBQWMsQ0FEbUI7QUFFakNDLFVBQVEsS0FGeUI7QUFHakNLLFFBQU07QUFIMkIsRUFBbEM7O0FBTUEsS0FBSUMsWUFBWVgsRUFBRSx3QkFBRixFQUE0QkcsS0FBNUIsQ0FBa0MsbUJBQWxDLElBQXlELENBQXpFO0FBQ0EsS0FBSVMsWUFBWVosRUFBRSx1Q0FBRixFQUEyQ2EsTUFBM0Q7O0FBRUEsS0FBSUYsWUFBWSxFQUFoQixFQUFvQkEsWUFBWSxNQUFNQSxTQUFsQjtBQUNwQixLQUFJQyxZQUFZLEVBQWhCLEVBQW9CQSxZQUFZLE1BQU1BLFNBQWxCOztBQUVuQlosR0FBRSw4Q0FBRixFQUFrRGMsSUFBbEQsQ0FBdURILFNBQXZEO0FBQ0FYLEdBQUUsOENBQUYsRUFBa0RjLElBQWxELENBQXVERixTQUF2RDs7QUFFRFosR0FBRSx3QkFBRixFQUE0QmUsRUFBNUIsQ0FBK0IsY0FBL0IsRUFBK0MsVUFBU0MsS0FBVCxFQUFnQmIsS0FBaEIsRUFBdUJjLFlBQXZCLEVBQXFDQyxTQUFyQyxFQUFnRDtBQUM5RjtBQUNBLE1BQUlDLGFBQWFELFlBQVksQ0FBN0I7QUFDQSxNQUFJQyxhQUFhLEVBQWpCLEVBQXFCQSxhQUFhLE1BQU1BLFVBQW5CO0FBQ3JCbkIsSUFBRSw4Q0FBRixFQUFrRGMsSUFBbEQsQ0FBdURLLFVBQXZEO0FBQ0EsRUFMRDs7QUFPQW5CLEdBQUUsOENBQUYsRUFBa0RvQixLQUFsRCxDQUF3RCxVQUFTQyxDQUFULEVBQVk7QUFDbkVBLElBQUVDLGNBQUY7QUFDQXRCLElBQUUsd0JBQUYsRUFBNEJHLEtBQTVCLENBQWtDLFdBQWxDO0FBQ0EsRUFIRDtBQUlBSCxHQUFFLDhDQUFGLEVBQWtEb0IsS0FBbEQsQ0FBd0QsVUFBU0MsQ0FBVCxFQUFZO0FBQ25FQSxJQUFFQyxjQUFGO0FBQ0F0QixJQUFFLHdCQUFGLEVBQTRCRyxLQUE1QixDQUFrQyxXQUFsQztBQUNBLEVBSEQ7QUFLQSxDQTFERCIsImZpbGUiOiJzbGljay5qcyIsInNvdXJjZXNDb250ZW50IjpbIiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xuXG5cdCQoJy5leHBlcmllbmNlcy1zbGlkZXInKS5zbGljayh7XG5cdFx0c2xpZGVzVG9TaG93OiAxLFxuXHRcdGFycm93czogZmFsc2UsXG5cdFx0YXV0b3BsYXk6IHRydWUsXG5cdFx0YXV0b3BsYXlTcGVlZDogNTAwMCxcblx0fSk7XG5cbiAgJCgnLnJlYXNvbnMtc2xpZGVyJykuc2xpY2soe1xuICAgIHNsaWRlc1RvU2hvdzogMS4wMSxcbiAgICBpbml0aWFsU2xpZGU6IDEsXG4gICAgaW5maW5pdGU6IHRydWUsXG4gICAgYXJyb3dzOiBmYWxzZSxcbiAgICBhdXRvcGxheTogdHJ1ZSxcbiAgICBhdXRvcGxheVNwZWVkOiA1MDAwLFxuXHR9KTtcblx0XG4gICQoJy5jb250ZW50LXNsaWRlcicpLnNsaWNrKHtcbiAgICBzbGlkZXNUb1Nob3c6IDEuMDEsXG4gICAgaW5pdGlhbFNsaWRlOiAxLFxuICAgIGluZmluaXRlOiB0cnVlLFxuICAgIGFycm93czogZmFsc2UsXG4gICAgLy9hdXRvcGxheTogdHJ1ZSxcbiAgICBhdXRvcGxheVNwZWVkOiA1MDAwLFxuICB9KTsgICAgICAgIFxuXG5cdCQoJy5jb250ZW50LXNsaWRlci1tb2JpbGUnKS5zbGljayh7XG5cdFx0c2xpZGVzVG9TaG93OiAxLFxuXHRcdGFycm93czogZmFsc2UsXG5cdFx0ZG90czogdHJ1ZVxuXHR9KTtcblxuXHR2YXIgY3VyTW9iaWxlID0gJCgnLmNvbnRlbnQtc2xpZGVyLW1vYmlsZScpLnNsaWNrKCdzbGlja0N1cnJlbnRTbGlkZScpICsgMTtcblx0dmFyIG1heE1vYmlsZSA9ICQoJy5jb250ZW50LXNsaWRlci1tb2JpbGUgLnNsaWNrLWRvdHMgbGknKS5sZW5ndGg7XG5cblx0aWYgKGN1ck1vYmlsZSA8IDEwKSBjdXJNb2JpbGUgPSAnMCcgKyBjdXJNb2JpbGU7XG5cdGlmIChtYXhNb2JpbGUgPCAxMCkgbWF4TW9iaWxlID0gJzAnICsgbWF4TW9iaWxlO1xuXG4gICQoJy5jb250ZW50LXNsaWRlci1jb250cm9scyAucGFnZS1pbmRpY2F0b3ItY3VyJykudGV4dChjdXJNb2JpbGUpO1xuICAkKCcuY29udGVudC1zbGlkZXItY29udHJvbHMgLnBhZ2UtaW5kaWNhdG9yLW1heCcpLnRleHQobWF4TW9iaWxlKTtcblxuXHQkKCcuY29udGVudC1zbGlkZXItbW9iaWxlJykub24oJ2JlZm9yZUNoYW5nZScsIGZ1bmN0aW9uKGV2ZW50LCBzbGljaywgY3VycmVudFNsaWRlLCBuZXh0U2xpZGUpIHtcblx0XHQvLyB1cGRhdGUgY291bnRlclxuXHRcdHZhciBzbGlkZUNvdW50ID0gbmV4dFNsaWRlICsgMTtcblx0XHRpZiAoc2xpZGVDb3VudCA8IDEwKSBzbGlkZUNvdW50ID0gJzAnICsgc2xpZGVDb3VudDtcblx0XHQkKCcuY29udGVudC1zbGlkZXItY29udHJvbHMgLnBhZ2UtaW5kaWNhdG9yLWN1cicpLnRleHQoc2xpZGVDb3VudCk7XG5cdH0pO1xuXG5cdCQoJy5jb250ZW50LXNsaWRlci1jb250cm9scyAucGFnZS1hcnJvd3MgYS5wcmV2JykuY2xpY2soZnVuY3Rpb24oZSkge1xuXHRcdGUucHJldmVudERlZmF1bHQoKTtcblx0XHQkKCcuY29udGVudC1zbGlkZXItbW9iaWxlJykuc2xpY2soJ3NsaWNrUHJldicpO1xuXHR9KTtcblx0JCgnLmNvbnRlbnQtc2xpZGVyLWNvbnRyb2xzIC5wYWdlLWFycm93cyBhLm5leHQnKS5jbGljayhmdW5jdGlvbihlKSB7XG5cdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdCQoJy5jb250ZW50LXNsaWRlci1tb2JpbGUnKS5zbGljaygnc2xpY2tOZXh0Jyk7XG5cdH0pO1xuXG59KTtcbiJdfQ==
