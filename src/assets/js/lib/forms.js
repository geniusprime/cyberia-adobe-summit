import $ from 'jquery';
'use strict';

//To use this script, a form must have the following:
//the class "adobe-campaign"
//the data-attribute "data-sname"
//the form elements should be wrapped in a class "form-elements"
//the success message should be wrapped in a class "success-message"
//the fail message should be wrapped in a class "fail-message"
//add data-clear to the form if you want to hide all form elements on submit

$(document).ready(function () {

function makeTimer() {
  //    var endTime = new Date("29 April 2018 9:56:00 GMT+01:00");
    var endTime = new Date("Jan 7 2021 9:56:00 GMT+01:00");
    endTime = (Date.parse(endTime) / 1000);

    var now = new Date();
    now = (Date.parse(now) / 1000);

    var timeLeft = endTime - now;

    var days = Math.floor(timeLeft / 86400);
    var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
    var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
    var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

    if (hours < "10") { hours = "0" + hours; }
    if (minutes < "10") { minutes = "0" + minutes; }
    if (seconds < "10") { seconds = "0" + seconds; }

    $(".days").html(days + "<span>Days</span>");
    $(".hours").html(hours + "<span>Hours</span>");
    $(".minutes").html(minutes + "<span>Minutes</span>");
    $(".seconds").html(seconds + "<span>Seconds</span>");
  }
  setInterval(function() { makeTimer(); }, 1000);

 function mobilePage() {
    //mobile fix
  if ($(window).width() < 375) {
    // if ($('.disable-slide-effect').css('width') == '0' || $('.disable-slide-effect').css('width') == '0px') {
       $('.page-home').addClass('disable-fixed');
   } else {
      $('.page-home').removeClass('disable-fixed');
    }
 }
//mobilePage();

$(window).resize(function(){
  //mobilePage();
});


  $.fn.isInViewport = function () {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();
    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();
    return elementBottom > viewportTop && elementTop < viewportBottom;
  };

  function getScrollbarWidth() {
    var outer = document.createElement('div');
    outer.style.visibility = 'hidden';
    outer.style.overflow = 'scroll';
    outer.style.msOverflowStyle = 'scrollbar';
    document.body.appendChild(outer);

    var inner = document.createElement('div');
    outer.appendChild(inner);

    var scrollbarWidth = outer.offsetWidth - inner.offsetWidth;

    outer.parentNode.removeChild(outer);

    return scrollbarWidth;
  }

  $('#mobile-nav-button').click(function () {
    var curScrollbarWidth = getScrollbarWidth();
    if ($(this).hasClass('is-active')) {
      if ($('.subnav-toggle').length) {
        $(this).css('padding-right', '+=' + curScrollbarWidth + 'px');
      }

      // hide subnav & restore default header title
      if ($('.subnav').is(':visible')) {
        // hide subnav
        $('.subnav').slideUp(400, function () {
          $('.mobile-nav').removeClass('subnav-active');
        });
      }
      // restore default header title
      $('.summit-logo.default').show();
      // $('.summit-logo.subnav-toggle').hide();
      $('.summit-logo-nav-wrapper').hide();
    } else {

      // if on a sub page with siblings or page with children, restore subnav
      if ($('.subnav-toggle').length) {
        $(this).css('padding-right', '-=' + curScrollbarWidth + 'px');
        $('.summit-logo.default').hide();
        // $('.summit-logo.subnav-toggle').show();
        $('.summit-logo-nav-wrapper').show();
      }
    }
  });


    //Mobile nav
  $('#mobile-nav-button').click(function () {
    $('#mobile-nav .dropdown').toggleClass('active');
    $('#mobile-nav-button').toggleClass('is-active');
    $('body').toggleClass('nav-open');
  });


  var addAnimationToPage = false;
  if ($('.anim-subtle-left').length) {
    addAnimationToPage = true;
    $('.anim-subtle-left').each(function () {
      if ($(this).isInViewport()) {
        $(this).find('img').addClass('animate');
      }
    });
  }

  var addMobileAnimationToPage = false;
  if ($('.anim-subtle-left-mobile').length) {
    addMobileAnimationToPage = true;
    $('.anim-subtle-left-mobile').each(function () {
      if ($(this).isInViewport()) {
        $(this).find('img').addClass('animate');
      }
    });
  }

  var addMarqueeAnimationToPage = false;
  if ($('.anim-subtle-left-marquee').length) {
    addMarqueeAnimationToPage = true;
    if ($('.anim-subtle-left-marquee').isInViewport()) {
      $('.anim-subtle-left-marquee').addClass('animate');
    }
  }

  var addMobileMarqueeAnimationToPage = false;
  if ($('.anim-subtle-left-marquee-mobile').length) {
    addMobileMarqueeAnimationToPage = true;
    if ($('.anim-subtle-left-marquee-mobile').isInViewport()) {
      $('.anim-subtle-left-marquee-mobile').addClass('animate');
    }
  }

  $(window).on('scroll', function () {
    if ($(this).scrollTop() > 20) {
      $('.nav-wrapper').addClass('with-bg');
    } else {
      $('.nav-wrapper').removeClass('with-bg');
    }

    if (addAnimationToPage) {
      $('.anim-subtle-left').each(function () {
        if ($(this).isInViewport()) {
          $(this).find('img').addClass('animate');
        } else {
          $(this).find('img').removeClass('animate');
        }
      });
    }

    if (addMobileAnimationToPage) {
      $('.anim-subtle-left-mobile').each(function () {
        if ($(this).isInViewport()) {
          $(this).find('img').addClass('animate');
        } else {
          $(this).find('img').removeClass('animate');
        }
      });
    }

    if (addMarqueeAnimationToPage) {
      if ($('.anim-subtle-left-marquee').isInViewport()) {
        $('.anim-subtle-left-marquee').addClass('animate');
      } else {
        $('.anim-subtle-left-marquee').removeClass('animate');
      }
    }

    if (addMobileMarqueeAnimationToPage) {
      if ($('.anim-subtle-left-marquee-mobile').isInViewport()) {
        $('.anim-subtle-left-marquee-mobile').addClass('animate');
      } else {
        $('.anim-subtle-left-marquee-mobile').removeClass('animate');
      }
    }
  });

  if ($('.sponsors-mobile').length) {
    $('.sponsors-mobile .accordion-title').click(function (e) {
      if ($(this).parent().hasClass('is-active')) {
        $('#sponsor-accordion').addClass('is-active');
      } else {
        $('#sponsor-accordion').removeClass('is-active');
      }
    });
  }

  if ($('.mi-keynote-speaker-random').length) {
    var miKeySpeakers = [{}, {
      firstName: 'Gary',
      lastName: 'Vaynerchuk',
      company: 'VaynerX'
    }, {
      firstName: 'Troy',
      lastName: 'Brown',
      company: 'Zumiez'
    }, {
      firstName: 'Jules',
      lastName: 'Pieri',
      company: 'The Grommet'
    }, {
      firstName: 'Gillian',
      lastName: 'Campbell',
      company: 'HP Inc.'
    }];
    var randomSpeakerId = Math.floor(Math.random() * Math.floor(4)) + 1;
    var keySpeakerSrc = $('.mi-keynote-speaker-random').attr('src').slice(0, -5);
    $('.mi-keynote-speaker-random').attr('src', keySpeakerSrc + randomSpeakerId + '.jpg');
    $('.mi-keynote-speaker-random-firstname').text(miKeySpeakers[randomSpeakerId].firstName);
    $('.mi-keynote-speaker-random-lastname').text(miKeySpeakers[randomSpeakerId].lastName);
    $('.mi-keynote-speaker-random-company').text(miKeySpeakers[randomSpeakerId].company);
  }

  if ($('.video-modal-trigger').length) {
    var closeBtn;

    (function () {
      var resetCloseBtn = function () {
        // adjust close-btn position
        var videoElPos = $('.video-content').position();
        $('.video-overlay .close-btn').css('top', videoElPos.top - closeBtn.topOffset).css('left', videoElPos.left - closeBtn.leftOffset + $('.video-content').width());
      };

      closeBtn = {
        topOffset: 16,
        leftOffset: 15
      };

      $(window).on('resize', function () {
        if ($('.video-overlay .close-btn').is(':visible')) {
          resetCloseBtn();
        }
        if ($('.disable-slide-effect').css('width') == '0' || $('.disable-slide-effect').css('width') == '0px') {
          $('.page-home').removeClass('disable-fixed');
        } else {
          $('.page-home').addClass('disable-fixed');
        }
      });

      if ($('.disable-slide-effect').css('width') == '0' || $('.disable-slide-effect').css('width') == '0px') {
        $('.page-home').removeClass('disable-fixed');
      } else {
        $('.page-home').addClass('disable-fixed');
      }

      $('.video-modal-trigger').on('click', function (e) {
        e.preventDefault();
        var videoEl = '.video-overlay[data-id=' + $(this).attr('data-target') + ']';
        $(videoEl + ' iframe').attr('src', $(videoEl + ' iframe').attr('data-src'));
        $(videoEl).stop().fadeIn();
        resetCloseBtn();
      });

      $('.video-overlay').on('click', function (e) {
        if (!$(e.target).hasClass('close-btn')) {
          var videoEl = '.video-overlay[data-id=' + $(this).attr('data-id') + ']';
          $(videoEl + ' iframe').attr('src', '');
          $(this).stop().fadeOut();
        }
      });

      $('.video-overlay .close-btn').on('click', function () {
        $('.video-overlay').stop().fadeOut();
        var videoEl = '.video-overlay[data-id=' + $(this).parent().parent().attr('data-id') + ']';
        $(videoEl + ' iframe').attr('src', '');
      });
    })();
  }


  $('form.adobe-campaign').each(function () {
    preventDeafaultBehaviour($(this));
  });

  $.fn.swipeDetector = function (options) {
    // States: 0 - no swipe, 1 - swipe started, 2 - swipe released
    var swipeState = 0;
    // Coordinates when swipe started
    var startX = 0;
    var startY = 0;
    // Distance of swipe
    var pixelOffsetX = 0;
    var pixelOffsetY = 0;
    // Target element which should detect swipes.
    var swipeTarget = this;
    var defaultSettings = {
      // Amount of pixels, when swipe don't count.
      swipeThreshold: 70,
      // Flag that indicates that plugin should react only on touch events.
      // Not on mouse events too.
      useOnlyTouch: false
    };

    // Initializer
    (function init() {
      options = $.extend(defaultSettings, options);
      // Support touch and mouse as well.
      swipeTarget.on('mousedown touchstart', swipeStart);
      $('html').on('mouseup touchend', swipeEnd);
      $('html').on('mousemove touchmove', swiping);
    })();

    function swipeStart(event) {
      if (options.useOnlyTouch && !event.originalEvent.touches) return;

      if (event.originalEvent.touches) event = event.originalEvent.touches[0];

      if (swipeState === 0) {
        swipeState = 1;
        startX = event.clientX;
        startY = event.clientY;
      }
    }

    function swipeEnd(event) {
      if (swipeState === 2) {
        swipeState = 0;

        if (Math.abs(pixelOffsetX) > Math.abs(pixelOffsetY) && Math.abs(pixelOffsetX) > options.swipeThreshold) {
          // Horizontal Swipe
          if (pixelOffsetX < 0) {
            swipeTarget.trigger($.Event('swipeLeft.sd'));
            //console.log('Left');
          } else {
            swipeTarget.trigger($.Event('swipeRight.sd'));
            //console.log('Right');
          }
        } else if (Math.abs(pixelOffsetY) > options.swipeThreshold) {
          // Vertical swipe
          if (pixelOffsetY < 0) {
            swipeTarget.trigger($.Event('swipeUp.sd'));
            //console.log('Up');
          } else {
            swipeTarget.trigger($.Event('swipeDown.sd'));
            //console.log('Down');
          }
        }
      }
    }

    function swiping(event) {
      // If swipe don't occuring, do nothing.
      if (swipeState !== 1) return;

      if (event.originalEvent.touches) {
        event = event.originalEvent.touches[0];
      }

      var swipeOffsetX = event.clientX - startX;
      var swipeOffsetY = event.clientY - startY;

      if (Math.abs(swipeOffsetX) > options.swipeThreshold || Math.abs(swipeOffsetY) > options.swipeThreshold) {
        swipeState = 2;
        pixelOffsetX = swipeOffsetX;
        pixelOffsetY = swipeOffsetY;
        //console.log(pixelOffsetX);
      }
    }

    return swipeTarget; // Return element available for chaining.
  };

  if ($('.homepage-content').length) {
    $('body').swipeDetector();
    $('body').on('swipeUp.sd', function () {
      if (!$('.page-home').hasClass('disable-fixed')) {
        nextContent();
      }
    });
    $('body').on('swipeDown.sd', function () {
      if (!$('.page-home').hasClass('disable-fixed')) {
        prevContent();
      }
    });
  }

  // init AOS
  //AOS.init();

  // header nav slide effects
  $('.header-nav > .is-dropdown-submenu-parent').hover(function () {
    $(this).children('ul.submenu').stop().slideDown(250);
  }, function () {
    $(this).children('ul.submenu').stop().slideUp(250);
  });

  // footer mailinglist signup button animation (morph to input)
  $('.button-morph-footer').click(function () {
    if (!$(this).hasClass('active')) {
      $(this).addClass('active');
      $(this).attr('placeholder', 'Email');
      setTimeout(function () {
        $('.button-morph-footer-submit').fadeIn();
      }, 200);
    }
  });

  // mailinglist signup submit
  var languagesClasses = ['default', 'blue', 'orange', 'green', 'pink', 'yellow'];
  var languagesText = ['Thank you', 'ありがとうございました', 'Спасибо', 'Gracias', 'Je vous remercie', '谢谢'];
  var languageStep = 1;
  $('.button-morph-footer-submit').click(function (e) {
    e.preventDefault();

    if ($('.button-morph-footer').val() == '') {
      alert('Please enter your email address.');
    } else {

      var form = $('.mailinglist-wrapper');
      var userEmail = { email: $('.button-morph-footer').val() };
      var sname = { sname: $(form).attr('data-sname') };
      var cn = $('.mailinglist-wrapper').find('.legal');
      var consent_notice = { consent_notice: cn.html() };
      var current_url = { 'current_url': window.location.href };
      // current_url['current_url'] = window.location.href
      var completeJson = $.extend(userEmail, sname, consent_notice, current_url);
      var stringJson = JSON.stringify(completeJson);

      $.ajax({
        type: "POST",
        url: "https://www.adobe.com/api2/subscribe_v1",
        data: stringJson,
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      }).done(function (data) {
        $('.button-morph-footer').val('');
        // success - morph into "Thank you" button
        $('.button-morph-footer-submit').fadeOut();
        $('.button-morph-footer').addClass('signup-success');
        $('.button-morph-footer').attr('placeholder', 'Thank you');

        // start text & colour rotation
        setInterval(function () {
          $('.button-morph-footer').removeClass('blue orange green pink yellow').addClass('fade-out');
          $('.button-morph-footer').attr('placeholder', languagesText[languageStep]).removeClass('fade-out');
          $('.button-morph-footer').addClass(languagesClasses[languageStep]);
          languageStep++;
          if (languageStep >= languagesClasses.length) languageStep = 0;
        }, 3000);

        // event tracks
        // fbq('track', 'Lead');
        // $('.footer-wrapper').append('<img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=447636&conversionId=1371052&fmt=gif" />');
        // gtag_report_conversion();
        // twttr.conversion.trackPid('o2fzu', { tw_sale_amount: 0, tw_order_quantity: 0 });
      }).fail(function (data) {
        alert('We could not add you to our mailing list. Please try again later.');

        $('.button-morph-footer').val('');
        $('.button-morph-footer-submit').hide();
        $('.button-morph-footer').removeClass('active');
        $('.button-morph-footer').attr('placeholder', 'Sign up');
      });
    }
  });

  // temporary mailinglist signup button animation reset
  $('.button-morph-footer-reset').click(function () {
    $('.button-morph-footer').removeClass('active').removeClass('signup-success');
    $('.button-morph-footer').attr('placeholder', 'Sign up');
    $('.button-morph-footer-submit').hide();
  });

  // mobile nav colour changes on toggle & add container class on page load if needed
  if ($('.accordion-item.is-active').length) {
    $('.accordion-container').addClass('has-active-links');
  }
  $('.nav-link').click(function (e) {
    var activeTopLinks = $('.accordion-item.is-active').length;
    if (activeTopLinks) {
      $('.accordion-container').addClass('has-active-links');
      if (!$(this).parent().prev().hasClass('is-active')) {
        $(this).parent().prev().addClass('is-active');
      } else {
        $(this).parent().prev().removeClass('is-active');
      }
    } else {
      $('.accordion-container').removeClass('has-active-links');
      $('.top-level-link.is-active').removeClass('is-active');
    }
  });

  // subnav toggle
  $('.subnav-toggle').click(function (e) {
    e.preventDefault();

    if ($('.subnav').is(':visible')) {
      // hide subnav
      $('.subnav').slideUp(400, function () {
        $('.mobile-nav').removeClass('subnav-active');
      });
    } else {
      // show subnav
      $('.mobile-nav').addClass('subnav-active');
      $('.subnav').slideDown();

      if (!$('.nav-wrapper').hasClass('with-bg')) {
        $('.nav-wrapper').addClass('with-bg');
      }
    }
  });

  // home page content
  var curContentId = 1;
  var maxContentId = $('.homepage-content').length;

  $('.page-indicator-cur').text(curContentId);
  $('.homepage-controls-wrapper .page-indicator-max').text(maxContentId);

  // home page: content controls
  for (var i = 1; i <= maxContentId; i++) {
    var activeClass = i == curContentId ? 'active' : '';
    $('.homepage-controls-wrapper .page-controls').append('<a href="#" class="slide' + i + ' ' + activeClass + '"><img class="circle-active" src="/assets/img/homepage/circle-active.png" /><img class="circle-inactive" src="/assets/img/homepage/circle-inactive.png" /></a>');
  }

  // home page: go to selected content slide
  $('.homepage-controls-wrapper .page-controls a').click(function (e) {
    e.preventDefault();

    var selectedContentId = parseInt($(this).attr('class').replace('slide', ''), 10);

    if (selectedContentId != curContentId) {
      $('.homepage-content.hpc' + curContentId).stop().slideUp(1000);
      curContentId = selectedContentId;
      $('.page-indicator-cur').text(curContentId);
      $('.homepage-controls-wrapper .page-controls a.active').removeClass('active');
      $(this).addClass('active');
      $('.homepage-content.hpc' + curContentId).stop().slideDown(1000);
    }
  });

  function prevContent() {
    if (curContentId > 1) {
      $('.homepage-content.hpc' + curContentId).stop().slideUp(1000);
      curContentId--;
      $('.page-indicator-cur').text(curContentId);
      $('.homepage-controls-wrapper .page-controls a.active').removeClass('active');
      $('.homepage-controls-wrapper .page-controls a.slide' + curContentId).addClass('active');
      $('.homepage-content.hpc' + curContentId).stop().slideDown(1000);
    }
  }

  // home page: go to previous content
  $('.homepage-controls-wrapper .page-arrows .prev').click(function (e) {
    e.preventDefault();
    prevContent();
  });

  function nextContent() {
    if (curContentId < maxContentId) {
      $('.homepage-content.hpc' + curContentId).stop().slideUp(1000);
      curContentId++;
      $('.page-indicator-cur').text(curContentId);
      $('.homepage-controls-wrapper .page-controls a.active').removeClass('active');
      $('.homepage-controls-wrapper .page-controls a.slide' + curContentId).addClass('active');
      $('.homepage-content.hpc' + curContentId).stop().slideDown(1000);
    }
  }

  // home page: go to next content
  $('.homepage-controls-wrapper .page-arrows .next').click(function (e) {
    e.preventDefault();
    nextContent();
  });

  if ($('.homepage-controls-wrapper').length) {
    $(document).keydown(function (e) {
      switch (e.which) {
        case 38:
          // up
          if (diff_seconds() > 1200 && !$('.page-home').hasClass('disable-fixed')) {
            lastScroll = new Date();
            prevContent();
          }
          break;

        case 40:
          // down
          if (diff_seconds() > 1200 && !$('.page-home').hasClass('disable-fixed')) {
            lastScroll = new Date();
            nextContent();
          }
          break;

        default:
          return; // exit this handler for other keys
      }
    });
  }

  // home page: scroll to content
  var lastScroll = new Date();

  function diff_seconds() {
    var dateNow = new Date();
    var timeDifference = dateNow.getTime() - lastScroll.getTime();
    return Math.abs(Math.round(timeDifference));
  }

  $(document).on('triggerScroll', function (e, scrollDirection) {
    if ($('.page-home').hasClass('disable-fixed')) {
      return true;
    }

    e.preventDefault();
    if (diff_seconds() > 1200) {
      lastScroll = new Date();
      if (scrollDirection == 1) {
        // scroll up, go to previous content
        if (curContentId > 1) {
          $('.homepage-content.hpc' + curContentId).stop().slideUp(1000);
          curContentId--;
          $('.page-indicator-cur').text(curContentId);
          $('.homepage-controls-wrapper .page-controls a.active').removeClass('active');
          $('.homepage-controls-wrapper .page-controls a.slide' + curContentId).addClass('active');
          $('.homepage-content.hpc' + curContentId).stop().slideDown(1000);
        }
      } else {
        // scroll down, go to next content
        if (curContentId < maxContentId) {
          $('.homepage-content.hpc' + curContentId).stop().slideUp(1000);
          curContentId++;
          $('.page-indicator-cur').text(curContentId);
          $('.homepage-controls-wrapper .page-controls a.active').removeClass('active');
          $('.homepage-controls-wrapper .page-controls a.slide' + curContentId).addClass('active');
          $('.homepage-content.hpc' + curContentId).stop().slideDown(1000);
        }
      }
    }
  });

  var supportsWheel = false;
  function wheel(e) {
    if (e.type == 'wheel') supportsWheel = true;else if (supportsWheel) return;
    $(document).trigger("triggerScroll", e.detail < 0 || e.wheelDelta > 0 || e.deltaY < 0 || e.deltaX < 0 ? 1 : -1);
  };

  document.addEventListener('wheel', wheel, { passive: false }, false);
  document.addEventListener('mousewheel', wheel, { passive: false }, false);
  document.addEventListener('DOMMouseScroll', wheel, { passive: false }, false);

  /*
    document.addEventListener('wheel', _.debounce(wheel, 250, { leading: true }), {passive: false}, false);
    document.addEventListener('mousewheel', _.debounce(wheel, 250, { leading: true }), {passive: false}, false);
    document.addEventListener('DOMMouseScroll', _.debounce(wheel, 250, { leading: true }), {passive: false}, false);
  */
});

function preventDeafaultBehaviour($form) {
  $form.bind('submit', function (event) {
    event.preventDefault();
  });
}

$('form.adobe-campaign').on("formvalid.zf.abide", function (event, frm) {
  var form = this;
  var json = ConvertFormToJSON(form);
  var sname = { sname: $(form).attr('data-sname') };
  var cn = $('.mailinglist').find('.legal');
  var consent_notice = { consent_notice: cn.html() };
  var current_url = { 'current_url': window.location.href };
  // current_url['current_url'] = window.location.href
  var completeJson = $.extend(json, sname, consent_notice, current_url);
  delete completeJson.non_human_check;
  var stringJson = JSON.stringify(completeJson);
  var isSummitOnlineSignup = $(this).hasClass('summit-online-signup-form');

  $.ajax({
    type: "POST",
    url: "https://www.adobe.com/api2/subscribe_v1",
    data: stringJson,
    contentType: "application/json; charset=utf-8",
    dataType: "json"
  }).done(function (data) {
    if (form.hasAttribute('data-clear')) {
      $(form).find('.form-elements').hide();
    }
    if (isSummitOnlineSignup) {
      $('.mailinglist-signup').hide();
      $('.mailinglist-thank-you').show();
    } else {
      $(form).find('.success-message').show();
    }
    $(form).reset();
    console.log(data);
  }).fail(function (data) {
    if (form.hasAttribute('data-clear')) {
      $(form).find('.form-elements').hide();
    }
    $(form).find('.fail-message').show();
    console.log(data);
  });

  return true;
});

function ConvertFormToJSON(form) {
  var array = jQuery(form).serializeArray();
  var json = {};

  jQuery.each(array, function () {
    json[this.name] = this.value || '';
  });

  return json;
}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZvcm1zLmpzIl0sIm5hbWVzIjpbIiQiLCJkb2N1bWVudCIsInJlYWR5IiwiZm4iLCJpc0luVmlld3BvcnQiLCJlbGVtZW50VG9wIiwib2Zmc2V0IiwidG9wIiwiZWxlbWVudEJvdHRvbSIsIm91dGVySGVpZ2h0Iiwidmlld3BvcnRUb3AiLCJ3aW5kb3ciLCJzY3JvbGxUb3AiLCJ2aWV3cG9ydEJvdHRvbSIsImhlaWdodCIsImdldFNjcm9sbGJhcldpZHRoIiwib3V0ZXIiLCJjcmVhdGVFbGVtZW50Iiwic3R5bGUiLCJ2aXNpYmlsaXR5Iiwib3ZlcmZsb3ciLCJtc092ZXJmbG93U3R5bGUiLCJib2R5IiwiYXBwZW5kQ2hpbGQiLCJpbm5lciIsInNjcm9sbGJhcldpZHRoIiwib2Zmc2V0V2lkdGgiLCJwYXJlbnROb2RlIiwicmVtb3ZlQ2hpbGQiLCJjbGljayIsImN1clNjcm9sbGJhcldpZHRoIiwiaGFzQ2xhc3MiLCJjc3MiLCJpcyIsInNsaWRlVXAiLCJyZW1vdmVDbGFzcyIsInNob3ciLCJoaWRlIiwibGVuZ3RoIiwiYWRkQW5pbWF0aW9uVG9QYWdlIiwiZWFjaCIsImZpbmQiLCJhZGRDbGFzcyIsImFkZE1vYmlsZUFuaW1hdGlvblRvUGFnZSIsImFkZE1hcnF1ZWVBbmltYXRpb25Ub1BhZ2UiLCJhZGRNb2JpbGVNYXJxdWVlQW5pbWF0aW9uVG9QYWdlIiwib24iLCJlIiwicGFyZW50IiwibWlLZXlTcGVha2VycyIsImZpcnN0TmFtZSIsImxhc3ROYW1lIiwiY29tcGFueSIsInJhbmRvbVNwZWFrZXJJZCIsIk1hdGgiLCJmbG9vciIsInJhbmRvbSIsImtleVNwZWFrZXJTcmMiLCJhdHRyIiwic2xpY2UiLCJ0ZXh0IiwiY2xvc2VCdG4iLCJyZXNldENsb3NlQnRuIiwidmlkZW9FbFBvcyIsInBvc2l0aW9uIiwidG9wT2Zmc2V0IiwibGVmdCIsImxlZnRPZmZzZXQiLCJ3aWR0aCIsInByZXZlbnREZWZhdWx0IiwidmlkZW9FbCIsInN0b3AiLCJmYWRlSW4iLCJ0YXJnZXQiLCJmYWRlT3V0IiwicHJldmVudERlYWZhdWx0QmVoYXZpb3VyIiwic3dpcGVEZXRlY3RvciIsIm9wdGlvbnMiLCJzd2lwZVN0YXRlIiwic3RhcnRYIiwic3RhcnRZIiwicGl4ZWxPZmZzZXRYIiwicGl4ZWxPZmZzZXRZIiwic3dpcGVUYXJnZXQiLCJkZWZhdWx0U2V0dGluZ3MiLCJzd2lwZVRocmVzaG9sZCIsInVzZU9ubHlUb3VjaCIsImluaXQiLCJleHRlbmQiLCJzd2lwZVN0YXJ0Iiwic3dpcGVFbmQiLCJzd2lwaW5nIiwiZXZlbnQiLCJvcmlnaW5hbEV2ZW50IiwidG91Y2hlcyIsImNsaWVudFgiLCJjbGllbnRZIiwiYWJzIiwidHJpZ2dlciIsIkV2ZW50Iiwic3dpcGVPZmZzZXRYIiwic3dpcGVPZmZzZXRZIiwibmV4dENvbnRlbnQiLCJwcmV2Q29udGVudCIsImhvdmVyIiwiY2hpbGRyZW4iLCJzbGlkZURvd24iLCJzZXRUaW1lb3V0IiwibGFuZ3VhZ2VzQ2xhc3NlcyIsImxhbmd1YWdlc1RleHQiLCJsYW5ndWFnZVN0ZXAiLCJ2YWwiLCJhbGVydCIsImZvcm0iLCJ1c2VyRW1haWwiLCJlbWFpbCIsInNuYW1lIiwiY24iLCJjb25zZW50X25vdGljZSIsImh0bWwiLCJjdXJyZW50X3VybCIsImxvY2F0aW9uIiwiaHJlZiIsImNvbXBsZXRlSnNvbiIsInN0cmluZ0pzb24iLCJKU09OIiwic3RyaW5naWZ5IiwiYWpheCIsInR5cGUiLCJ1cmwiLCJkYXRhIiwiY29udGVudFR5cGUiLCJkYXRhVHlwZSIsImRvbmUiLCJzZXRJbnRlcnZhbCIsImZicSIsImFwcGVuZCIsImd0YWdfcmVwb3J0X2NvbnZlcnNpb24iLCJ0d3R0ciIsImNvbnZlcnNpb24iLCJ0cmFja1BpZCIsInR3X3NhbGVfYW1vdW50IiwidHdfb3JkZXJfcXVhbnRpdHkiLCJmYWlsIiwiYWN0aXZlVG9wTGlua3MiLCJwcmV2IiwiY3VyQ29udGVudElkIiwibWF4Q29udGVudElkIiwiaSIsImFjdGl2ZUNsYXNzIiwic2VsZWN0ZWRDb250ZW50SWQiLCJwYXJzZUludCIsInJlcGxhY2UiLCJrZXlkb3duIiwid2hpY2giLCJkaWZmX3NlY29uZHMiLCJsYXN0U2Nyb2xsIiwiRGF0ZSIsImRhdGVOb3ciLCJ0aW1lRGlmZmVyZW5jZSIsImdldFRpbWUiLCJyb3VuZCIsInNjcm9sbERpcmVjdGlvbiIsInN1cHBvcnRzV2hlZWwiLCJ3aGVlbCIsImRldGFpbCIsIndoZWVsRGVsdGEiLCJkZWx0YVkiLCJkZWx0YVgiLCJhZGRFdmVudExpc3RlbmVyIiwicGFzc2l2ZSIsIiRmb3JtIiwiYmluZCIsImZybSIsImpzb24iLCJDb252ZXJ0Rm9ybVRvSlNPTiIsIm5vbl9odW1hbl9jaGVjayIsImlzU3VtbWl0T25saW5lU2lnbnVwIiwiaGFzQXR0cmlidXRlIiwicmVzZXQiLCJjb25zb2xlIiwibG9nIiwiYXJyYXkiLCJqUXVlcnkiLCJzZXJpYWxpemVBcnJheSIsIm5hbWUiLCJ2YWx1ZSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQUEsRUFBRUMsUUFBRixFQUFZQyxLQUFaLENBQWtCLFlBQVU7O0FBRTFCRixJQUFFRyxFQUFGLENBQUtDLFlBQUwsR0FBb0IsWUFBVztBQUM3QixRQUFJQyxhQUFhTCxFQUFFLElBQUYsRUFBUU0sTUFBUixHQUFpQkMsR0FBbEM7QUFDQSxRQUFJQyxnQkFBZ0JILGFBQWFMLEVBQUUsSUFBRixFQUFRUyxXQUFSLEVBQWpDO0FBQ0EsUUFBSUMsY0FBY1YsRUFBRVcsTUFBRixFQUFVQyxTQUFWLEVBQWxCO0FBQ0EsUUFBSUMsaUJBQWlCSCxjQUFjVixFQUFFVyxNQUFGLEVBQVVHLE1BQVYsRUFBbkM7QUFDQSxXQUFPTixnQkFBZ0JFLFdBQWhCLElBQStCTCxhQUFhUSxjQUFuRDtBQUNELEdBTkQ7O0FBUUEsV0FBU0UsaUJBQVQsR0FBNkI7QUFDM0IsUUFBTUMsUUFBUWYsU0FBU2dCLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBZDtBQUNBRCxVQUFNRSxLQUFOLENBQVlDLFVBQVosR0FBeUIsUUFBekI7QUFDQUgsVUFBTUUsS0FBTixDQUFZRSxRQUFaLEdBQXVCLFFBQXZCO0FBQ0FKLFVBQU1FLEtBQU4sQ0FBWUcsZUFBWixHQUE4QixXQUE5QjtBQUNBcEIsYUFBU3FCLElBQVQsQ0FBY0MsV0FBZCxDQUEwQlAsS0FBMUI7O0FBRUEsUUFBTVEsUUFBUXZCLFNBQVNnQixhQUFULENBQXVCLEtBQXZCLENBQWQ7QUFDQUQsVUFBTU8sV0FBTixDQUFrQkMsS0FBbEI7O0FBRUEsUUFBTUMsaUJBQWtCVCxNQUFNVSxXQUFOLEdBQW9CRixNQUFNRSxXQUFsRDs7QUFFQVYsVUFBTVcsVUFBTixDQUFpQkMsV0FBakIsQ0FBNkJaLEtBQTdCOztBQUVBLFdBQU9TLGNBQVA7QUFDRDtBQUNEekIsSUFBRSxvQkFBRixFQUF3QjZCLEtBQXhCLENBQThCLFlBQVc7QUFDdkMsUUFBSUMsb0JBQW9CZixtQkFBeEI7QUFDQSxRQUFJZixFQUFFLElBQUYsRUFBUStCLFFBQVIsQ0FBaUIsV0FBakIsQ0FBSixFQUFtQztBQUNqQy9CLFFBQUUsSUFBRixFQUFRZ0MsR0FBUixDQUFZLGVBQVosRUFBNkIsT0FBT0YsaUJBQVAsR0FBMkIsSUFBeEQ7O0FBRUE7QUFDQSxVQUFJOUIsRUFBRSxTQUFGLEVBQWFpQyxFQUFiLENBQWdCLFVBQWhCLENBQUosRUFBaUM7QUFDL0I7QUFDQWpDLFVBQUUsU0FBRixFQUFha0MsT0FBYixDQUFxQixHQUFyQixFQUEwQixZQUFXO0FBQ25DbEMsWUFBRSxhQUFGLEVBQWlCbUMsV0FBakIsQ0FBNkIsZUFBN0I7QUFDRCxTQUZEO0FBR0Q7QUFDRDtBQUNBbkMsUUFBRSxzQkFBRixFQUEwQm9DLElBQTFCO0FBQ0E7QUFDQXBDLFFBQUUsMEJBQUYsRUFBOEJxQyxJQUE5QjtBQUNELEtBZEQsTUFjTztBQUNMckMsUUFBRSxJQUFGLEVBQVFnQyxHQUFSLENBQVksZUFBWixFQUE2QixPQUFPRixpQkFBUCxHQUEyQixJQUF4RDs7QUFFQTtBQUNBLFVBQUk5QixFQUFFLGdCQUFGLEVBQW9Cc0MsTUFBeEIsRUFBZ0M7QUFDOUJ0QyxVQUFFLHNCQUFGLEVBQTBCcUMsSUFBMUI7QUFDQTtBQUNBckMsVUFBRSwwQkFBRixFQUE4Qm9DLElBQTlCO0FBQ0Q7QUFDRjtBQUNGLEdBMUJEOztBQTRCQSxNQUFJRyxxQkFBcUIsS0FBekI7QUFDQSxNQUFJdkMsRUFBRSxtQkFBRixFQUF1QnNDLE1BQTNCLEVBQW1DO0FBQ2pDQyx5QkFBcUIsSUFBckI7QUFDQXZDLE1BQUUsbUJBQUYsRUFBdUJ3QyxJQUF2QixDQUE0QixZQUFXO0FBQ3JDLFVBQUl4QyxFQUFFLElBQUYsRUFBUUksWUFBUixFQUFKLEVBQTRCO0FBQzFCSixVQUFFLElBQUYsRUFBUXlDLElBQVIsQ0FBYSxLQUFiLEVBQW9CQyxRQUFwQixDQUE2QixTQUE3QjtBQUNEO0FBQ0YsS0FKRDtBQUtEOztBQUVELE1BQUlDLDJCQUEyQixLQUEvQjtBQUNBLE1BQUkzQyxFQUFFLDBCQUFGLEVBQThCc0MsTUFBbEMsRUFBMEM7QUFDeENLLCtCQUEyQixJQUEzQjtBQUNBM0MsTUFBRSwwQkFBRixFQUE4QndDLElBQTlCLENBQW1DLFlBQVc7QUFDNUMsVUFBSXhDLEVBQUUsSUFBRixFQUFRSSxZQUFSLEVBQUosRUFBNEI7QUFDMUJKLFVBQUUsSUFBRixFQUFReUMsSUFBUixDQUFhLEtBQWIsRUFBb0JDLFFBQXBCLENBQTZCLFNBQTdCO0FBQ0Q7QUFDRixLQUpEO0FBS0Q7O0FBRUQsTUFBSUUsNEJBQTRCLEtBQWhDO0FBQ0EsTUFBSTVDLEVBQUUsMkJBQUYsRUFBK0JzQyxNQUFuQyxFQUEyQztBQUN6Q00sZ0NBQTRCLElBQTVCO0FBQ0EsUUFBSTVDLEVBQUUsMkJBQUYsRUFBK0JJLFlBQS9CLEVBQUosRUFBbUQ7QUFDakRKLFFBQUUsMkJBQUYsRUFBK0IwQyxRQUEvQixDQUF3QyxTQUF4QztBQUNEO0FBQ0Y7O0FBRUQsTUFBSUcsa0NBQWtDLEtBQXRDO0FBQ0EsTUFBSTdDLEVBQUUsa0NBQUYsRUFBc0NzQyxNQUExQyxFQUFrRDtBQUNoRE8sc0NBQWtDLElBQWxDO0FBQ0EsUUFBSTdDLEVBQUUsa0NBQUYsRUFBc0NJLFlBQXRDLEVBQUosRUFBMEQ7QUFDeERKLFFBQUUsa0NBQUYsRUFBc0MwQyxRQUF0QyxDQUErQyxTQUEvQztBQUNEO0FBQ0Y7O0FBRUQxQyxJQUFFVyxNQUFGLEVBQVVtQyxFQUFWLENBQWEsUUFBYixFQUF1QixZQUFVO0FBQy9CLFFBQUc5QyxFQUFFLElBQUYsRUFBUVksU0FBUixLQUFzQixFQUF6QixFQUE2QjtBQUMzQlosUUFBRSxjQUFGLEVBQWtCMEMsUUFBbEIsQ0FBMkIsU0FBM0I7QUFDRCxLQUZELE1BRU87QUFDTDFDLFFBQUUsY0FBRixFQUFrQm1DLFdBQWxCLENBQThCLFNBQTlCO0FBQ0Q7O0FBRUQsUUFBSUksa0JBQUosRUFBd0I7QUFDdEJ2QyxRQUFFLG1CQUFGLEVBQXVCd0MsSUFBdkIsQ0FBNEIsWUFBVztBQUNyQyxZQUFJeEMsRUFBRSxJQUFGLEVBQVFJLFlBQVIsRUFBSixFQUE0QjtBQUMxQkosWUFBRSxJQUFGLEVBQVF5QyxJQUFSLENBQWEsS0FBYixFQUFvQkMsUUFBcEIsQ0FBNkIsU0FBN0I7QUFDRCxTQUZELE1BRU87QUFDTDFDLFlBQUUsSUFBRixFQUFReUMsSUFBUixDQUFhLEtBQWIsRUFBb0JOLFdBQXBCLENBQWdDLFNBQWhDO0FBQ0Q7QUFDRixPQU5EO0FBT0Q7O0FBRUQsUUFBSVEsd0JBQUosRUFBOEI7QUFDNUIzQyxRQUFFLDBCQUFGLEVBQThCd0MsSUFBOUIsQ0FBbUMsWUFBVztBQUM1QyxZQUFJeEMsRUFBRSxJQUFGLEVBQVFJLFlBQVIsRUFBSixFQUE0QjtBQUMxQkosWUFBRSxJQUFGLEVBQVF5QyxJQUFSLENBQWEsS0FBYixFQUFvQkMsUUFBcEIsQ0FBNkIsU0FBN0I7QUFDRCxTQUZELE1BRU87QUFDTDFDLFlBQUUsSUFBRixFQUFReUMsSUFBUixDQUFhLEtBQWIsRUFBb0JOLFdBQXBCLENBQWdDLFNBQWhDO0FBQ0Q7QUFDRixPQU5EO0FBT0Q7O0FBRUQsUUFBSVMseUJBQUosRUFBK0I7QUFDN0IsVUFBSTVDLEVBQUUsMkJBQUYsRUFBK0JJLFlBQS9CLEVBQUosRUFBbUQ7QUFDakRKLFVBQUUsMkJBQUYsRUFBK0IwQyxRQUEvQixDQUF3QyxTQUF4QztBQUNELE9BRkQsTUFFTztBQUNMMUMsVUFBRSwyQkFBRixFQUErQm1DLFdBQS9CLENBQTJDLFNBQTNDO0FBQ0Q7QUFDRjs7QUFFRCxRQUFJVSwrQkFBSixFQUFxQztBQUNuQyxVQUFJN0MsRUFBRSxrQ0FBRixFQUFzQ0ksWUFBdEMsRUFBSixFQUEwRDtBQUN4REosVUFBRSxrQ0FBRixFQUFzQzBDLFFBQXRDLENBQStDLFNBQS9DO0FBQ0QsT0FGRCxNQUVPO0FBQ0wxQyxVQUFFLGtDQUFGLEVBQXNDbUMsV0FBdEMsQ0FBa0QsU0FBbEQ7QUFDRDtBQUNGO0FBQ0YsR0ExQ0Q7O0FBNENBLE1BQUluQyxFQUFFLGtCQUFGLEVBQXNCc0MsTUFBMUIsRUFBa0M7QUFDaEN0QyxNQUFFLG1DQUFGLEVBQXVDNkIsS0FBdkMsQ0FBNkMsVUFBU2tCLENBQVQsRUFBWTtBQUN2RCxVQUFJL0MsRUFBRSxJQUFGLEVBQVFnRCxNQUFSLEdBQWlCakIsUUFBakIsQ0FBMEIsV0FBMUIsQ0FBSixFQUE0QztBQUMxQy9CLFVBQUUsb0JBQUYsRUFBd0IwQyxRQUF4QixDQUFpQyxXQUFqQztBQUNELE9BRkQsTUFFTztBQUNMMUMsVUFBRSxvQkFBRixFQUF3Qm1DLFdBQXhCLENBQW9DLFdBQXBDO0FBQ0Q7QUFDRixLQU5EO0FBT0Q7O0FBRUQsTUFBSW5DLEVBQUUsNEJBQUYsRUFBZ0NzQyxNQUFwQyxFQUE0QztBQUMxQyxRQUFJVyxnQkFBZ0IsQ0FDbEIsRUFEa0IsRUFHbEI7QUFDRUMsaUJBQVcsTUFEYjtBQUVFQyxnQkFBVSxZQUZaO0FBR0VDLGVBQVM7QUFIWCxLQUhrQixFQVFsQjtBQUNFRixpQkFBVyxNQURiO0FBRUVDLGdCQUFVLE9BRlo7QUFHRUMsZUFBUztBQUhYLEtBUmtCLEVBYWxCO0FBQ0VGLGlCQUFXLE9BRGI7QUFFRUMsZ0JBQVUsT0FGWjtBQUdFQyxlQUFTO0FBSFgsS0Fia0IsRUFrQmxCO0FBQ0VGLGlCQUFXLFNBRGI7QUFFRUMsZ0JBQVUsVUFGWjtBQUdFQyxlQUFTO0FBSFgsS0FsQmtCLENBQXBCO0FBd0JBLFFBQUlDLGtCQUFrQkMsS0FBS0MsS0FBTCxDQUFXRCxLQUFLRSxNQUFMLEtBQWdCRixLQUFLQyxLQUFMLENBQVcsQ0FBWCxDQUEzQixJQUE0QyxDQUFsRTtBQUNBLFFBQUlFLGdCQUFnQnpELEVBQUUsNEJBQUYsRUFBZ0MwRCxJQUFoQyxDQUFxQyxLQUFyQyxFQUE0Q0MsS0FBNUMsQ0FBa0QsQ0FBbEQsRUFBcUQsQ0FBQyxDQUF0RCxDQUFwQjtBQUNBM0QsTUFBRSw0QkFBRixFQUFnQzBELElBQWhDLENBQXFDLEtBQXJDLEVBQTRDRCxnQkFBZ0JKLGVBQWhCLEdBQWtDLE1BQTlFO0FBQ0FyRCxNQUFFLHNDQUFGLEVBQTBDNEQsSUFBMUMsQ0FBK0NYLGNBQWNJLGVBQWQsRUFBK0JILFNBQTlFO0FBQ0FsRCxNQUFFLHFDQUFGLEVBQXlDNEQsSUFBekMsQ0FBOENYLGNBQWNJLGVBQWQsRUFBK0JGLFFBQTdFO0FBQ0FuRCxNQUFFLG9DQUFGLEVBQXdDNEQsSUFBeEMsQ0FBNkNYLGNBQWNJLGVBQWQsRUFBK0JELE9BQTVFO0FBQ0Q7O0FBRUQsTUFBSXBELEVBQUUsc0JBQUYsRUFBMEJzQyxNQUE5QixFQUFzQztBQUFBLFFBQ2hDdUIsUUFEZ0M7O0FBQUE7QUFBQSxVQU0zQkMsYUFOMkIsR0FNcEMsWUFBeUI7QUFDdkI7QUFDQSxZQUFJQyxhQUFhL0QsRUFBRSxnQkFBRixFQUFvQmdFLFFBQXBCLEVBQWpCO0FBQ0FoRSxVQUFFLDJCQUFGLEVBQStCZ0MsR0FBL0IsQ0FBbUMsS0FBbkMsRUFBMEMrQixXQUFXeEQsR0FBWCxHQUFpQnNELFNBQVNJLFNBQXBFLEVBQStFakMsR0FBL0UsQ0FBbUYsTUFBbkYsRUFBMkYrQixXQUFXRyxJQUFYLEdBQWtCTCxTQUFTTSxVQUEzQixHQUF3Q25FLEVBQUUsZ0JBQUYsRUFBb0JvRSxLQUFwQixFQUFuSTtBQUNELE9BVm1DOztBQUNoQ1AsaUJBQVc7QUFDYkksbUJBQVcsRUFERTtBQUViRSxvQkFBWTtBQUZDLE9BRHFCOzs7QUFZcENuRSxRQUFFVyxNQUFGLEVBQVVtQyxFQUFWLENBQWEsUUFBYixFQUF1QixZQUFXO0FBQ2hDLFlBQUk5QyxFQUFFLDJCQUFGLEVBQStCaUMsRUFBL0IsQ0FBa0MsVUFBbEMsQ0FBSixFQUFtRDtBQUNqRDZCO0FBQ0Q7QUFDRCxZQUFJOUQsRUFBRSx1QkFBRixFQUEyQmdDLEdBQTNCLENBQStCLE9BQS9CLEtBQTJDLEdBQTNDLElBQWtEaEMsRUFBRSx1QkFBRixFQUEyQmdDLEdBQTNCLENBQStCLE9BQS9CLEtBQTJDLEtBQWpHLEVBQXdHO0FBQ3RHaEMsWUFBRSxZQUFGLEVBQWdCbUMsV0FBaEIsQ0FBNEIsZUFBNUI7QUFDRCxTQUZELE1BRU87QUFDTG5DLFlBQUUsWUFBRixFQUFnQjBDLFFBQWhCLENBQXlCLGVBQXpCO0FBQ0Q7QUFDRixPQVREOztBQVdBLFVBQUkxQyxFQUFFLHVCQUFGLEVBQTJCZ0MsR0FBM0IsQ0FBK0IsT0FBL0IsS0FBMkMsR0FBM0MsSUFBa0RoQyxFQUFFLHVCQUFGLEVBQTJCZ0MsR0FBM0IsQ0FBK0IsT0FBL0IsS0FBMkMsS0FBakcsRUFBd0c7QUFDdEdoQyxVQUFFLFlBQUYsRUFBZ0JtQyxXQUFoQixDQUE0QixlQUE1QjtBQUNELE9BRkQsTUFFTztBQUNMbkMsVUFBRSxZQUFGLEVBQWdCMEMsUUFBaEIsQ0FBeUIsZUFBekI7QUFDRDs7QUFFRDFDLFFBQUUsc0JBQUYsRUFBMEI4QyxFQUExQixDQUE2QixPQUE3QixFQUFzQyxVQUFTQyxDQUFULEVBQVk7QUFDaERBLFVBQUVzQixjQUFGO0FBQ0EsWUFBSUMsVUFBVSw0QkFBNEJ0RSxFQUFFLElBQUYsRUFBUTBELElBQVIsQ0FBYSxhQUFiLENBQTVCLEdBQTBELEdBQXhFO0FBQ0ExRCxVQUFFc0UsVUFBVSxTQUFaLEVBQXVCWixJQUF2QixDQUE0QixLQUE1QixFQUFtQzFELEVBQUVzRSxVQUFVLFNBQVosRUFBdUJaLElBQXZCLENBQTRCLFVBQTVCLENBQW5DO0FBQ0ExRCxVQUFFc0UsT0FBRixFQUFXQyxJQUFYLEdBQWtCQyxNQUFsQjtBQUNBVjtBQUNELE9BTkQ7O0FBUUE5RCxRQUFFLGdCQUFGLEVBQW9COEMsRUFBcEIsQ0FBdUIsT0FBdkIsRUFBZ0MsVUFBU0MsQ0FBVCxFQUFZO0FBQzFDLFlBQUksQ0FBQy9DLEVBQUUrQyxFQUFFMEIsTUFBSixFQUFZMUMsUUFBWixDQUFxQixXQUFyQixDQUFMLEVBQXdDO0FBQ3RDLGNBQUl1QyxVQUFVLDRCQUE0QnRFLEVBQUUsSUFBRixFQUFRMEQsSUFBUixDQUFhLFNBQWIsQ0FBNUIsR0FBc0QsR0FBcEU7QUFDQTFELFlBQUVzRSxVQUFVLFNBQVosRUFBdUJaLElBQXZCLENBQTRCLEtBQTVCLEVBQW1DLEVBQW5DO0FBQ0ExRCxZQUFFLElBQUYsRUFBUXVFLElBQVIsR0FBZUcsT0FBZjtBQUNEO0FBQ0YsT0FORDs7QUFRQTFFLFFBQUUsMkJBQUYsRUFBK0I4QyxFQUEvQixDQUFrQyxPQUFsQyxFQUEyQyxZQUFXO0FBQ3BEOUMsVUFBRSxnQkFBRixFQUFvQnVFLElBQXBCLEdBQTJCRyxPQUEzQjtBQUNBLFlBQUlKLFVBQVUsNEJBQTRCdEUsRUFBRSxJQUFGLEVBQVFnRCxNQUFSLEdBQWlCQSxNQUFqQixHQUEwQlUsSUFBMUIsQ0FBK0IsU0FBL0IsQ0FBNUIsR0FBd0UsR0FBdEY7QUFDQTFELFVBQUVzRSxVQUFVLFNBQVosRUFBdUJaLElBQXZCLENBQTRCLEtBQTVCLEVBQW1DLEVBQW5DO0FBQ0QsT0FKRDtBQTdDb0M7QUFrRHJDOztBQUVGMUQsSUFBRSxxQkFBRixFQUF5QndDLElBQXpCLENBQThCLFlBQVU7QUFDdkNtQyw2QkFBeUIzRSxFQUFFLElBQUYsQ0FBekI7QUFDQyxHQUZGOztBQUlDQSxJQUFFRyxFQUFGLENBQUt5RSxhQUFMLEdBQXFCLFVBQVVDLE9BQVYsRUFBbUI7QUFDdEM7QUFDQSxRQUFJQyxhQUFhLENBQWpCO0FBQ0E7QUFDQSxRQUFJQyxTQUFTLENBQWI7QUFDQSxRQUFJQyxTQUFTLENBQWI7QUFDQTtBQUNBLFFBQUlDLGVBQWUsQ0FBbkI7QUFDQSxRQUFJQyxlQUFlLENBQW5CO0FBQ0E7QUFDQSxRQUFJQyxjQUFjLElBQWxCO0FBQ0EsUUFBSUMsa0JBQWtCO0FBQ3BCO0FBQ0FDLHNCQUFnQixFQUZJO0FBR3BCO0FBQ0E7QUFDQUMsb0JBQWM7QUFMTSxLQUF0Qjs7QUFRQTtBQUNBLEtBQUMsU0FBU0MsSUFBVCxHQUFnQjtBQUNmVixnQkFBVTdFLEVBQUV3RixNQUFGLENBQVNKLGVBQVQsRUFBMEJQLE9BQTFCLENBQVY7QUFDQTtBQUNBTSxrQkFBWXJDLEVBQVosQ0FBZSxzQkFBZixFQUF1QzJDLFVBQXZDO0FBQ0F6RixRQUFFLE1BQUYsRUFBVThDLEVBQVYsQ0FBYSxrQkFBYixFQUFpQzRDLFFBQWpDO0FBQ0ExRixRQUFFLE1BQUYsRUFBVThDLEVBQVYsQ0FBYSxxQkFBYixFQUFvQzZDLE9BQXBDO0FBQ0QsS0FORDs7QUFRQSxhQUFTRixVQUFULENBQW9CRyxLQUFwQixFQUEyQjtBQUN6QixVQUFJZixRQUFRUyxZQUFSLElBQXdCLENBQUNNLE1BQU1DLGFBQU4sQ0FBb0JDLE9BQWpELEVBQ0U7O0FBRUYsVUFBSUYsTUFBTUMsYUFBTixDQUFvQkMsT0FBeEIsRUFDRUYsUUFBUUEsTUFBTUMsYUFBTixDQUFvQkMsT0FBcEIsQ0FBNEIsQ0FBNUIsQ0FBUjs7QUFFRixVQUFJaEIsZUFBZSxDQUFuQixFQUFzQjtBQUNwQkEscUJBQWEsQ0FBYjtBQUNBQyxpQkFBU2EsTUFBTUcsT0FBZjtBQUNBZixpQkFBU1ksTUFBTUksT0FBZjtBQUNEO0FBQ0Y7O0FBRUQsYUFBU04sUUFBVCxDQUFrQkUsS0FBbEIsRUFBeUI7QUFDdkIsVUFBSWQsZUFBZSxDQUFuQixFQUFzQjtBQUNwQkEscUJBQWEsQ0FBYjs7QUFFQSxZQUFJeEIsS0FBSzJDLEdBQUwsQ0FBU2hCLFlBQVQsSUFBeUIzQixLQUFLMkMsR0FBTCxDQUFTZixZQUFULENBQXpCLElBQ0Q1QixLQUFLMkMsR0FBTCxDQUFTaEIsWUFBVCxJQUF5QkosUUFBUVEsY0FEcEMsRUFDb0Q7QUFBRTtBQUNwRCxjQUFJSixlQUFlLENBQW5CLEVBQXNCO0FBQ3BCRSx3QkFBWWUsT0FBWixDQUFvQmxHLEVBQUVtRyxLQUFGLENBQVEsY0FBUixDQUFwQjtBQUNBO0FBQ0QsV0FIRCxNQUdPO0FBQ0xoQix3QkFBWWUsT0FBWixDQUFvQmxHLEVBQUVtRyxLQUFGLENBQVEsZUFBUixDQUFwQjtBQUNBO0FBQ0Q7QUFDRixTQVRELE1BU08sSUFBSTdDLEtBQUsyQyxHQUFMLENBQVNmLFlBQVQsSUFBeUJMLFFBQVFRLGNBQXJDLEVBQXFEO0FBQUU7QUFDNUQsY0FBSUgsZUFBZSxDQUFuQixFQUFzQjtBQUNwQkMsd0JBQVllLE9BQVosQ0FBb0JsRyxFQUFFbUcsS0FBRixDQUFRLFlBQVIsQ0FBcEI7QUFDQTtBQUNELFdBSEQsTUFHTztBQUNMaEIsd0JBQVllLE9BQVosQ0FBb0JsRyxFQUFFbUcsS0FBRixDQUFRLGNBQVIsQ0FBcEI7QUFDQTtBQUNEO0FBQ0Y7QUFDRjtBQUNGOztBQUVELGFBQVNSLE9BQVQsQ0FBaUJDLEtBQWpCLEVBQXdCO0FBQ3RCO0FBQ0EsVUFBSWQsZUFBZSxDQUFuQixFQUFzQjs7QUFFdEIsVUFBSWMsTUFBTUMsYUFBTixDQUFvQkMsT0FBeEIsRUFBaUM7QUFDL0JGLGdCQUFRQSxNQUFNQyxhQUFOLENBQW9CQyxPQUFwQixDQUE0QixDQUE1QixDQUFSO0FBQ0Q7O0FBRUQsVUFBSU0sZUFBZVIsTUFBTUcsT0FBTixHQUFnQmhCLE1BQW5DO0FBQ0EsVUFBSXNCLGVBQWVULE1BQU1JLE9BQU4sR0FBZ0JoQixNQUFuQzs7QUFFQSxVQUFLMUIsS0FBSzJDLEdBQUwsQ0FBU0csWUFBVCxJQUF5QnZCLFFBQVFRLGNBQWxDLElBQ0MvQixLQUFLMkMsR0FBTCxDQUFTSSxZQUFULElBQXlCeEIsUUFBUVEsY0FEdEMsRUFDdUQ7QUFDckRQLHFCQUFhLENBQWI7QUFDQUcsdUJBQWVtQixZQUFmO0FBQ0FsQix1QkFBZW1CLFlBQWY7QUFDQTtBQUNEO0FBQ0Y7O0FBRUQsV0FBT2xCLFdBQVAsQ0F2RnNDLENBdUZsQjtBQUNyQixHQXhGRDs7QUEwRkEsTUFBSW5GLEVBQUUsbUJBQUYsRUFBdUJzQyxNQUEzQixFQUFtQztBQUNqQ3RDLE1BQUUsTUFBRixFQUFVNEUsYUFBVjtBQUNBNUUsTUFBRSxNQUFGLEVBQVU4QyxFQUFWLENBQWEsWUFBYixFQUEyQixZQUFXO0FBQ3BDLFVBQUksQ0FBQzlDLEVBQUUsWUFBRixFQUFnQitCLFFBQWhCLENBQXlCLGVBQXpCLENBQUwsRUFBZ0Q7QUFDOUN1RTtBQUNEO0FBQ0YsS0FKRDtBQUtBdEcsTUFBRSxNQUFGLEVBQVU4QyxFQUFWLENBQWEsY0FBYixFQUE2QixZQUFXO0FBQ3RDLFVBQUksQ0FBQzlDLEVBQUUsWUFBRixFQUFnQitCLFFBQWhCLENBQXlCLGVBQXpCLENBQUwsRUFBZ0Q7QUFDOUN3RTtBQUNEO0FBQ0YsS0FKRDtBQUtEOztBQUVEO0FBQ0E7O0FBRUE7QUFDQXZHLElBQUUsMkNBQUYsRUFBK0N3RyxLQUEvQyxDQUFxRCxZQUFXO0FBQzlEeEcsTUFBRSxJQUFGLEVBQVF5RyxRQUFSLENBQWlCLFlBQWpCLEVBQStCbEMsSUFBL0IsR0FBc0NtQyxTQUF0QyxDQUFnRCxHQUFoRDtBQUNELEdBRkQsRUFFRyxZQUFXO0FBQ1oxRyxNQUFFLElBQUYsRUFBUXlHLFFBQVIsQ0FBaUIsWUFBakIsRUFBK0JsQyxJQUEvQixHQUFzQ3JDLE9BQXRDLENBQThDLEdBQTlDO0FBQ0QsR0FKRDs7QUFNQTtBQUNBbEMsSUFBRSxzQkFBRixFQUEwQjZCLEtBQTFCLENBQWdDLFlBQVc7QUFDekMsUUFBSSxDQUFDN0IsRUFBRSxJQUFGLEVBQVErQixRQUFSLENBQWlCLFFBQWpCLENBQUwsRUFBaUM7QUFDL0IvQixRQUFFLElBQUYsRUFBUTBDLFFBQVIsQ0FBaUIsUUFBakI7QUFDQTFDLFFBQUUsSUFBRixFQUFRMEQsSUFBUixDQUFhLGFBQWIsRUFBNEIsT0FBNUI7QUFDQWlELGlCQUFXLFlBQVc7QUFDcEIzRyxVQUFFLDZCQUFGLEVBQWlDd0UsTUFBakM7QUFDRCxPQUZELEVBRUcsR0FGSDtBQUdEO0FBQ0YsR0FSRDs7QUFVQTtBQUNBLE1BQUlvQyxtQkFBbUIsQ0FBQyxTQUFELEVBQVksTUFBWixFQUFvQixRQUFwQixFQUE4QixPQUE5QixFQUF1QyxNQUF2QyxFQUErQyxRQUEvQyxDQUF2QjtBQUNBLE1BQUlDLGdCQUFnQixDQUFDLFdBQUQsRUFBYyxhQUFkLEVBQTZCLFNBQTdCLEVBQXdDLFNBQXhDLEVBQW1ELGtCQUFuRCxFQUF1RSxJQUF2RSxDQUFwQjtBQUNBLE1BQUlDLGVBQWUsQ0FBbkI7QUFDQTlHLElBQUUsNkJBQUYsRUFBaUM2QixLQUFqQyxDQUF1QyxVQUFTa0IsQ0FBVCxFQUFZO0FBQ2pEQSxNQUFFc0IsY0FBRjs7QUFFQSxRQUFJckUsRUFBRSxzQkFBRixFQUEwQitHLEdBQTFCLE1BQW1DLEVBQXZDLEVBQTJDO0FBQ3pDQyxZQUFNLGtDQUFOO0FBQ0QsS0FGRCxNQUVPOztBQUVMLFVBQUlDLE9BQU9qSCxFQUFFLHNCQUFGLENBQVg7QUFDQSxVQUFJa0gsWUFBWSxFQUFFQyxPQUFPbkgsRUFBRSxzQkFBRixFQUEwQitHLEdBQTFCLEVBQVQsRUFBaEI7QUFDQSxVQUFJSyxRQUFRLEVBQUVBLE9BQU9wSCxFQUFFaUgsSUFBRixFQUFRdkQsSUFBUixDQUFhLFlBQWIsQ0FBVCxFQUFaO0FBQ0EsVUFBSTJELEtBQUtySCxFQUFFLHNCQUFGLEVBQTBCeUMsSUFBMUIsQ0FBK0IsUUFBL0IsQ0FBVDtBQUNBLFVBQUk2RSxpQkFBaUIsRUFBRUEsZ0JBQWdCRCxHQUFHRSxJQUFILEVBQWxCLEVBQXJCO0FBQ0EsVUFBSUMsY0FBYSxFQUFFLGVBQWU3RyxPQUFPOEcsUUFBUCxDQUFnQkMsSUFBakMsRUFBakI7QUFDQTtBQUNBLFVBQUlDLGVBQWUzSCxFQUFFd0YsTUFBRixDQUFVMEIsU0FBVixFQUFxQkUsS0FBckIsRUFBNEJFLGNBQTVCLEVBQTRDRSxXQUE1QyxDQUFuQjtBQUNBLFVBQUlJLGFBQWFDLEtBQUtDLFNBQUwsQ0FBZUgsWUFBZixDQUFqQjs7QUFFQTNILFFBQUUrSCxJQUFGLENBQU87QUFDTEMsY0FBTSxNQUREO0FBRUxDLGFBQUsseUNBRkE7QUFHTEMsY0FBTU4sVUFIRDtBQUlMTyxxQkFBWSxpQ0FKUDtBQUtMQyxrQkFBVTtBQUxMLE9BQVAsRUFNR0MsSUFOSCxDQU1RLFVBQVNILElBQVQsRUFBZTtBQUNyQmxJLFVBQUUsc0JBQUYsRUFBMEIrRyxHQUExQixDQUE4QixFQUE5QjtBQUNBO0FBQ0EvRyxVQUFFLDZCQUFGLEVBQWlDMEUsT0FBakM7QUFDQTFFLFVBQUUsc0JBQUYsRUFBMEIwQyxRQUExQixDQUFtQyxnQkFBbkM7QUFDQTFDLFVBQUUsc0JBQUYsRUFBMEIwRCxJQUExQixDQUErQixhQUEvQixFQUE4QyxXQUE5Qzs7QUFFQTtBQUNBNEUsb0JBQVksWUFBVztBQUNyQnRJLFlBQUUsc0JBQUYsRUFBMEJtQyxXQUExQixDQUFzQywrQkFBdEMsRUFBdUVPLFFBQXZFLENBQWdGLFVBQWhGO0FBQ0ExQyxZQUFFLHNCQUFGLEVBQTBCMEQsSUFBMUIsQ0FBK0IsYUFBL0IsRUFBOENtRCxjQUFjQyxZQUFkLENBQTlDLEVBQTJFM0UsV0FBM0UsQ0FBdUYsVUFBdkY7QUFDQW5DLFlBQUUsc0JBQUYsRUFBMEIwQyxRQUExQixDQUFtQ2tFLGlCQUFpQkUsWUFBakIsQ0FBbkM7QUFDQUE7QUFDQSxjQUFJQSxnQkFBZ0JGLGlCQUFpQnRFLE1BQXJDLEVBQTZDd0UsZUFBZSxDQUFmO0FBQzlDLFNBTkQsRUFNRyxJQU5IOztBQVFBO0FBQ0F5QixZQUFJLE9BQUosRUFBYSxNQUFiO0FBQ0F2SSxVQUFFLGlCQUFGLEVBQXFCd0ksTUFBckIsQ0FBNEIsOElBQTVCO0FBQ0FDO0FBQ0FDLGNBQU1DLFVBQU4sQ0FBaUJDLFFBQWpCLENBQTBCLE9BQTFCLEVBQW1DLEVBQUVDLGdCQUFnQixDQUFsQixFQUFxQkMsbUJBQW1CLENBQXhDLEVBQW5DO0FBQ0QsT0EzQkQsRUEyQkdDLElBM0JILENBMkJRLFVBQVNiLElBQVQsRUFBZTtBQUNyQmxCLGNBQU0sbUVBQU47O0FBRUFoSCxVQUFFLHNCQUFGLEVBQTBCK0csR0FBMUIsQ0FBOEIsRUFBOUI7QUFDQS9HLFVBQUUsNkJBQUYsRUFBaUNxQyxJQUFqQztBQUNBckMsVUFBRSxzQkFBRixFQUEwQm1DLFdBQTFCLENBQXNDLFFBQXRDO0FBQ0FuQyxVQUFFLHNCQUFGLEVBQTBCMEQsSUFBMUIsQ0FBK0IsYUFBL0IsRUFBOEMsU0FBOUM7QUFDRCxPQWxDRDtBQW9DRDtBQUNGLEdBdEREOztBQXdEQTtBQUNBMUQsSUFBRSw0QkFBRixFQUFnQzZCLEtBQWhDLENBQXNDLFlBQVc7QUFDL0M3QixNQUFFLHNCQUFGLEVBQTBCbUMsV0FBMUIsQ0FBc0MsUUFBdEMsRUFBZ0RBLFdBQWhELENBQTRELGdCQUE1RDtBQUNBbkMsTUFBRSxzQkFBRixFQUEwQjBELElBQTFCLENBQStCLGFBQS9CLEVBQThDLFNBQTlDO0FBQ0ExRCxNQUFFLDZCQUFGLEVBQWlDcUMsSUFBakM7QUFDRCxHQUpEOztBQU1BO0FBQ0EsTUFBSXJDLEVBQUUsMkJBQUYsRUFBK0JzQyxNQUFuQyxFQUEyQztBQUN6Q3RDLE1BQUUsc0JBQUYsRUFBMEIwQyxRQUExQixDQUFtQyxrQkFBbkM7QUFDRDtBQUNEMUMsSUFBRSxXQUFGLEVBQWU2QixLQUFmLENBQXFCLFVBQVNrQixDQUFULEVBQVk7QUFDL0IsUUFBSWlHLGlCQUFpQmhKLEVBQUUsMkJBQUYsRUFBK0JzQyxNQUFwRDtBQUNBLFFBQUkwRyxjQUFKLEVBQW9CO0FBQ2xCaEosUUFBRSxzQkFBRixFQUEwQjBDLFFBQTFCLENBQW1DLGtCQUFuQztBQUNBLFVBQUksQ0FBQzFDLEVBQUUsSUFBRixFQUFRZ0QsTUFBUixHQUFpQmlHLElBQWpCLEdBQXdCbEgsUUFBeEIsQ0FBaUMsV0FBakMsQ0FBTCxFQUFvRDtBQUNsRC9CLFVBQUUsSUFBRixFQUFRZ0QsTUFBUixHQUFpQmlHLElBQWpCLEdBQXdCdkcsUUFBeEIsQ0FBaUMsV0FBakM7QUFDRCxPQUZELE1BRU87QUFDTDFDLFVBQUUsSUFBRixFQUFRZ0QsTUFBUixHQUFpQmlHLElBQWpCLEdBQXdCOUcsV0FBeEIsQ0FBb0MsV0FBcEM7QUFDRDtBQUNGLEtBUEQsTUFPTztBQUNMbkMsUUFBRSxzQkFBRixFQUEwQm1DLFdBQTFCLENBQXNDLGtCQUF0QztBQUNBbkMsUUFBRSwyQkFBRixFQUErQm1DLFdBQS9CLENBQTJDLFdBQTNDO0FBQ0Q7QUFDRixHQWJEOztBQWVBO0FBQ0FuQyxJQUFFLGdCQUFGLEVBQW9CNkIsS0FBcEIsQ0FBMEIsVUFBU2tCLENBQVQsRUFBWTtBQUNwQ0EsTUFBRXNCLGNBQUY7O0FBRUEsUUFBSXJFLEVBQUUsU0FBRixFQUFhaUMsRUFBYixDQUFnQixVQUFoQixDQUFKLEVBQWlDO0FBQy9CO0FBQ0FqQyxRQUFFLFNBQUYsRUFBYWtDLE9BQWIsQ0FBcUIsR0FBckIsRUFBMEIsWUFBVztBQUNuQ2xDLFVBQUUsYUFBRixFQUFpQm1DLFdBQWpCLENBQTZCLGVBQTdCO0FBQ0QsT0FGRDtBQUdELEtBTEQsTUFLTztBQUNMO0FBQ0FuQyxRQUFFLGFBQUYsRUFBaUIwQyxRQUFqQixDQUEwQixlQUExQjtBQUNBMUMsUUFBRSxTQUFGLEVBQWEwRyxTQUFiOztBQUVBLFVBQUksQ0FBQzFHLEVBQUUsY0FBRixFQUFrQitCLFFBQWxCLENBQTJCLFNBQTNCLENBQUwsRUFBNEM7QUFDMUMvQixVQUFFLGNBQUYsRUFBa0IwQyxRQUFsQixDQUEyQixTQUEzQjtBQUNEO0FBQ0Y7QUFDRixHQWpCRDs7QUFtQkE7QUFDQSxNQUFJd0csZUFBZSxDQUFuQjtBQUNBLE1BQUlDLGVBQWVuSixFQUFFLG1CQUFGLEVBQXVCc0MsTUFBMUM7O0FBRUF0QyxJQUFFLHFCQUFGLEVBQXlCNEQsSUFBekIsQ0FBOEJzRixZQUE5QjtBQUNBbEosSUFBRSxxQkFBRixFQUF5QjRELElBQXpCLENBQThCdUYsWUFBOUI7O0FBRUE7QUFDQSxPQUFJLElBQUlDLElBQUksQ0FBWixFQUFlQSxLQUFLRCxZQUFwQixFQUFrQ0MsR0FBbEMsRUFBdUM7QUFDckMsUUFBSUMsY0FBZUQsS0FBS0YsWUFBTixHQUFzQixRQUF0QixHQUFpQyxFQUFuRDtBQUNBbEosTUFBRSwyQ0FBRixFQUErQ3dJLE1BQS9DLENBQXNELDZCQUE2QlksQ0FBN0IsR0FBaUMsR0FBakMsR0FBdUNDLFdBQXZDLEdBQXFELDhLQUEzRztBQUNEOztBQUVEO0FBQ0FySixJQUFFLDZDQUFGLEVBQWlENkIsS0FBakQsQ0FBdUQsVUFBU2tCLENBQVQsRUFBWTtBQUNqRUEsTUFBRXNCLGNBQUY7O0FBRUEsUUFBSWlGLG9CQUFvQkMsU0FBU3ZKLEVBQUUsSUFBRixFQUFRMEQsSUFBUixDQUFhLE9BQWIsRUFBc0I4RixPQUF0QixDQUE4QixPQUE5QixFQUF1QyxFQUF2QyxDQUFULEVBQXFELEVBQXJELENBQXhCOztBQUVBLFFBQUlGLHFCQUFxQkosWUFBekIsRUFBdUM7QUFDckNsSixRQUFFLDBCQUEwQmtKLFlBQTVCLEVBQTBDM0UsSUFBMUMsR0FBaURyQyxPQUFqRCxDQUF5RCxJQUF6RDtBQUNBZ0gscUJBQWVJLGlCQUFmO0FBQ0F0SixRQUFFLHFCQUFGLEVBQXlCNEQsSUFBekIsQ0FBOEJzRixZQUE5QjtBQUNBbEosUUFBRSxvREFBRixFQUF3RG1DLFdBQXhELENBQW9FLFFBQXBFO0FBQ0FuQyxRQUFFLElBQUYsRUFBUTBDLFFBQVIsQ0FBaUIsUUFBakI7QUFDQTFDLFFBQUUsMEJBQTBCa0osWUFBNUIsRUFBMEMzRSxJQUExQyxHQUFpRG1DLFNBQWpELENBQTJELElBQTNEO0FBQ0Q7QUFDRixHQWJEOztBQWVBLFdBQVNILFdBQVQsR0FBdUI7QUFDckIsUUFBSTJDLGVBQWUsQ0FBbkIsRUFBc0I7QUFDcEJsSixRQUFFLDBCQUEwQmtKLFlBQTVCLEVBQTBDM0UsSUFBMUMsR0FBaURyQyxPQUFqRCxDQUF5RCxJQUF6RDtBQUNBZ0g7QUFDQWxKLFFBQUUscUJBQUYsRUFBeUI0RCxJQUF6QixDQUE4QnNGLFlBQTlCO0FBQ0FsSixRQUFFLG9EQUFGLEVBQXdEbUMsV0FBeEQsQ0FBb0UsUUFBcEU7QUFDQW5DLFFBQUUsc0RBQXNEa0osWUFBeEQsRUFBc0V4RyxRQUF0RSxDQUErRSxRQUEvRTtBQUNBMUMsUUFBRSwwQkFBMEJrSixZQUE1QixFQUEwQzNFLElBQTFDLEdBQWlEbUMsU0FBakQsQ0FBMkQsSUFBM0Q7QUFDRDtBQUNGOztBQUVEO0FBQ0ExRyxJQUFFLCtDQUFGLEVBQW1ENkIsS0FBbkQsQ0FBeUQsVUFBU2tCLENBQVQsRUFBWTtBQUNuRUEsTUFBRXNCLGNBQUY7QUFDQWtDO0FBQ0QsR0FIRDs7QUFLQSxXQUFTRCxXQUFULEdBQXVCO0FBQ3JCLFFBQUk0QyxlQUFlQyxZQUFuQixFQUFpQztBQUMvQm5KLFFBQUUsMEJBQTBCa0osWUFBNUIsRUFBMEMzRSxJQUExQyxHQUFpRHJDLE9BQWpELENBQXlELElBQXpEO0FBQ0FnSDtBQUNBbEosUUFBRSxxQkFBRixFQUF5QjRELElBQXpCLENBQThCc0YsWUFBOUI7QUFDQWxKLFFBQUUsb0RBQUYsRUFBd0RtQyxXQUF4RCxDQUFvRSxRQUFwRTtBQUNBbkMsUUFBRSxzREFBc0RrSixZQUF4RCxFQUFzRXhHLFFBQXRFLENBQStFLFFBQS9FO0FBQ0ExQyxRQUFFLDBCQUEwQmtKLFlBQTVCLEVBQTBDM0UsSUFBMUMsR0FBaURtQyxTQUFqRCxDQUEyRCxJQUEzRDtBQUNEO0FBQ0Y7O0FBRUQ7QUFDQTFHLElBQUUsK0NBQUYsRUFBbUQ2QixLQUFuRCxDQUF5RCxVQUFTa0IsQ0FBVCxFQUFZO0FBQ25FQSxNQUFFc0IsY0FBRjtBQUNBaUM7QUFDRCxHQUhEOztBQUtBLE1BQUl0RyxFQUFFLDRCQUFGLEVBQWdDc0MsTUFBcEMsRUFBNEM7QUFDMUN0QyxNQUFFQyxRQUFGLEVBQVl3SixPQUFaLENBQW9CLFVBQVMxRyxDQUFULEVBQVk7QUFDOUIsY0FBT0EsRUFBRTJHLEtBQVQ7QUFDSSxhQUFLLEVBQUw7QUFBUztBQUNQLGNBQUlDLGlCQUFpQixJQUFqQixJQUF5QixDQUFDM0osRUFBRSxZQUFGLEVBQWdCK0IsUUFBaEIsQ0FBeUIsZUFBekIsQ0FBOUIsRUFBeUU7QUFDdkU2SCx5QkFBYSxJQUFJQyxJQUFKLEVBQWI7QUFDQXREO0FBQ0Q7QUFDSDs7QUFFQSxhQUFLLEVBQUw7QUFBUztBQUNQLGNBQUlvRCxpQkFBaUIsSUFBakIsSUFBeUIsQ0FBQzNKLEVBQUUsWUFBRixFQUFnQitCLFFBQWhCLENBQXlCLGVBQXpCLENBQTlCLEVBQXlFO0FBQ3ZFNkgseUJBQWEsSUFBSUMsSUFBSixFQUFiO0FBQ0F2RDtBQUNEO0FBQ0g7O0FBRUE7QUFBUyxpQkFmYixDQWVxQjtBQWZyQjtBQWlCRCxLQWxCRDtBQW1CRDs7QUFFRDtBQUNBLE1BQUlzRCxhQUFhLElBQUlDLElBQUosRUFBakI7O0FBRUEsV0FBU0YsWUFBVCxHQUF3QjtBQUN0QixRQUFJRyxVQUFVLElBQUlELElBQUosRUFBZDtBQUNBLFFBQUlFLGlCQUFrQkQsUUFBUUUsT0FBUixLQUFvQkosV0FBV0ksT0FBWCxFQUExQztBQUNBLFdBQU8xRyxLQUFLMkMsR0FBTCxDQUFTM0MsS0FBSzJHLEtBQUwsQ0FBV0YsY0FBWCxDQUFULENBQVA7QUFDRDs7QUFFRC9KLElBQUVDLFFBQUYsRUFBWTZDLEVBQVosQ0FBZSxlQUFmLEVBQWdDLFVBQVNDLENBQVQsRUFBWW1ILGVBQVosRUFBNkI7QUFDM0QsUUFBSWxLLEVBQUUsWUFBRixFQUFnQitCLFFBQWhCLENBQXlCLGVBQXpCLENBQUosRUFBK0M7QUFDN0MsYUFBTyxJQUFQO0FBQ0Q7O0FBRURnQixNQUFFc0IsY0FBRjtBQUNBLFFBQUlzRixpQkFBaUIsSUFBckIsRUFBMkI7QUFDekJDLG1CQUFhLElBQUlDLElBQUosRUFBYjtBQUNBLFVBQUlLLG1CQUFtQixDQUF2QixFQUEwQjtBQUN4QjtBQUNBLFlBQUloQixlQUFlLENBQW5CLEVBQXNCO0FBQ3BCbEosWUFBRSwwQkFBMEJrSixZQUE1QixFQUEwQzNFLElBQTFDLEdBQWlEckMsT0FBakQsQ0FBeUQsSUFBekQ7QUFDQWdIO0FBQ0FsSixZQUFFLHFCQUFGLEVBQXlCNEQsSUFBekIsQ0FBOEJzRixZQUE5QjtBQUNBbEosWUFBRSxvREFBRixFQUF3RG1DLFdBQXhELENBQW9FLFFBQXBFO0FBQ0FuQyxZQUFFLHNEQUFzRGtKLFlBQXhELEVBQXNFeEcsUUFBdEUsQ0FBK0UsUUFBL0U7QUFDQTFDLFlBQUUsMEJBQTBCa0osWUFBNUIsRUFBMEMzRSxJQUExQyxHQUFpRG1DLFNBQWpELENBQTJELElBQTNEO0FBQ0Q7QUFDRixPQVZELE1BVU87QUFDTDtBQUNBLFlBQUl3QyxlQUFlQyxZQUFuQixFQUFpQztBQUMvQm5KLFlBQUUsMEJBQTBCa0osWUFBNUIsRUFBMEMzRSxJQUExQyxHQUFpRHJDLE9BQWpELENBQXlELElBQXpEO0FBQ0FnSDtBQUNBbEosWUFBRSxxQkFBRixFQUF5QjRELElBQXpCLENBQThCc0YsWUFBOUI7QUFDQWxKLFlBQUUsb0RBQUYsRUFBd0RtQyxXQUF4RCxDQUFvRSxRQUFwRTtBQUNBbkMsWUFBRSxzREFBc0RrSixZQUF4RCxFQUFzRXhHLFFBQXRFLENBQStFLFFBQS9FO0FBQ0ExQyxZQUFFLDBCQUEwQmtKLFlBQTVCLEVBQTBDM0UsSUFBMUMsR0FBaURtQyxTQUFqRCxDQUEyRCxJQUEzRDtBQUNEO0FBQ0Y7QUFDRjtBQUNGLEdBOUJEOztBQWdDQSxNQUFJeUQsZ0JBQWdCLEtBQXBCO0FBQ0EsV0FBU0MsS0FBVCxDQUFlckgsQ0FBZixFQUFrQjtBQUNoQixRQUFJQSxFQUFFaUYsSUFBRixJQUFVLE9BQWQsRUFBdUJtQyxnQkFBZ0IsSUFBaEIsQ0FBdkIsS0FDSyxJQUFJQSxhQUFKLEVBQW1CO0FBQ3hCbkssTUFBRUMsUUFBRixFQUFZaUcsT0FBWixDQUFvQixlQUFwQixFQUFzQ25ELEVBQUVzSCxNQUFGLEdBQVMsQ0FBVCxJQUFjdEgsRUFBRXVILFVBQUYsR0FBYSxDQUEzQixJQUFnQ3ZILEVBQUV3SCxNQUFGLEdBQVcsQ0FBM0MsSUFBZ0R4SCxFQUFFeUgsTUFBRixHQUFXLENBQTVELEdBQWlFLENBQWpFLEdBQXFFLENBQUMsQ0FBM0c7QUFDRDs7QUFFRHZLLFdBQVN3SyxnQkFBVCxDQUEwQixPQUExQixFQUFtQ0wsS0FBbkMsRUFBMEMsRUFBQ00sU0FBUyxLQUFWLEVBQTFDLEVBQTRELEtBQTVEO0FBQ0F6SyxXQUFTd0ssZ0JBQVQsQ0FBMEIsWUFBMUIsRUFBd0NMLEtBQXhDLEVBQStDLEVBQUNNLFNBQVMsS0FBVixFQUEvQyxFQUFpRSxLQUFqRTtBQUNBekssV0FBU3dLLGdCQUFULENBQTBCLGdCQUExQixFQUE0Q0wsS0FBNUMsRUFBbUQsRUFBQ00sU0FBUyxLQUFWLEVBQW5ELEVBQXFFLEtBQXJFOztBQUVGOzs7OztBQU1DLENBL2xCRDs7QUFpbUJBLFNBQVMvRix3QkFBVCxDQUFrQ2dHLEtBQWxDLEVBQXdDO0FBQ3ZDQSxRQUFNQyxJQUFOLENBQVcsUUFBWCxFQUFxQixVQUFTaEYsS0FBVCxFQUFlO0FBQ25DQSxVQUFNdkIsY0FBTjtBQUNBLEdBRkQ7QUFHQTs7QUFHRHJFLEVBQUUscUJBQUYsRUFBeUI4QyxFQUF6QixDQUE0QixvQkFBNUIsRUFBa0QsVUFBUzhDLEtBQVQsRUFBZWlGLEdBQWYsRUFBb0I7QUFDcEUsTUFBSTVELE9BQU8sSUFBWDtBQUNFLE1BQUk2RCxPQUFPQyxrQkFBa0I5RCxJQUFsQixDQUFYO0FBQ0EsTUFBSUcsUUFBUSxFQUFFQSxPQUFPcEgsRUFBRWlILElBQUYsRUFBUXZELElBQVIsQ0FBYSxZQUFiLENBQVQsRUFBWjtBQUNBLE1BQUkyRCxLQUFLckgsRUFBRSxjQUFGLEVBQWtCeUMsSUFBbEIsQ0FBdUIsUUFBdkIsQ0FBVDtBQUNBLE1BQUk2RSxpQkFBaUIsRUFBRUEsZ0JBQWdCRCxHQUFHRSxJQUFILEVBQWxCLEVBQXJCO0FBQ0EsTUFBSUMsY0FBYSxFQUFFLGVBQWU3RyxPQUFPOEcsUUFBUCxDQUFnQkMsSUFBakMsRUFBakI7QUFDQTtBQUNBLE1BQUlDLGVBQWUzSCxFQUFFd0YsTUFBRixDQUFVc0YsSUFBVixFQUFnQjFELEtBQWhCLEVBQXVCRSxjQUF2QixFQUF1Q0UsV0FBdkMsQ0FBbkI7QUFDQSxTQUFPRyxhQUFhcUQsZUFBcEI7QUFDQSxNQUFJcEQsYUFBYUMsS0FBS0MsU0FBTCxDQUFlSCxZQUFmLENBQWpCO0FBQ0EsTUFBSXNELHVCQUF1QmpMLEVBQUUsSUFBRixFQUFRK0IsUUFBUixDQUFpQiwyQkFBakIsQ0FBM0I7O0FBRUEvQixJQUFFK0gsSUFBRixDQUFPO0FBQ0xDLFVBQU0sTUFERDtBQUVMQyxTQUFLLHlDQUZBO0FBR0xDLFVBQU1OLFVBSEQ7QUFJTE8saUJBQVksaUNBSlA7QUFLTEMsY0FBVTtBQUxMLEdBQVAsRUFNR0MsSUFOSCxDQU1RLFVBQVNILElBQVQsRUFBZTtBQUN4QixRQUFJakIsS0FBS2lFLFlBQUwsQ0FBa0IsWUFBbEIsQ0FBSixFQUFxQztBQUNwQ2xMLFFBQUVpSCxJQUFGLEVBQVF4RSxJQUFSLENBQWEsZ0JBQWIsRUFBK0JKLElBQS9CO0FBQ0c7QUFDRCxRQUFJNEksb0JBQUosRUFBMEI7QUFDeEJqTCxRQUFFLHFCQUFGLEVBQXlCcUMsSUFBekI7QUFDQXJDLFFBQUUsd0JBQUYsRUFBNEJvQyxJQUE1QjtBQUNELEtBSEQsTUFHTztBQUNMcEMsUUFBRWlILElBQUYsRUFBUXhFLElBQVIsQ0FBYSxrQkFBYixFQUFpQ0wsSUFBakM7QUFDRDtBQUNKcEMsTUFBRWlILElBQUYsRUFBUWtFLEtBQVI7QUFDR0MsWUFBUUMsR0FBUixDQUFZbkQsSUFBWjtBQUNELEdBbEJELEVBa0JHYSxJQWxCSCxDQWtCUSxVQUFTYixJQUFULEVBQWU7QUFDeEIsUUFBSWpCLEtBQUtpRSxZQUFMLENBQWtCLFlBQWxCLENBQUosRUFBcUM7QUFDcENsTCxRQUFFaUgsSUFBRixFQUFReEUsSUFBUixDQUFhLGdCQUFiLEVBQStCSixJQUEvQjtBQUNBO0FBQ0RyQyxNQUFFaUgsSUFBRixFQUFReEUsSUFBUixDQUFhLGVBQWIsRUFBOEJMLElBQTlCO0FBQ0dnSixZQUFRQyxHQUFSLENBQVluRCxJQUFaO0FBQ0QsR0F4QkQ7O0FBMEJBLFNBQU8sSUFBUDtBQUNILENBeENEOztBQTBDQSxTQUFTNkMsaUJBQVQsQ0FBMkI5RCxJQUEzQixFQUFnQztBQUM5QixNQUFJcUUsUUFBUUMsT0FBT3RFLElBQVAsRUFBYXVFLGNBQWIsRUFBWjtBQUNBLE1BQUlWLE9BQU8sRUFBWDs7QUFFQVMsU0FBTy9JLElBQVAsQ0FBWThJLEtBQVosRUFBbUIsWUFBVztBQUM1QlIsU0FBSyxLQUFLVyxJQUFWLElBQWtCLEtBQUtDLEtBQUwsSUFBYyxFQUFoQztBQUNELEdBRkQ7O0FBSUEsU0FBT1osSUFBUDtBQUNEIiwiZmlsZSI6ImZvcm1zLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy9UbyB1c2UgdGhpcyBzY3JpcHQsIGEgZm9ybSBtdXN0IGhhdmUgdGhlIGZvbGxvd2luZzpcbi8vdGhlIGNsYXNzIFwiYWRvYmUtY2FtcGFpZ25cIlxuLy90aGUgZGF0YS1hdHRyaWJ1dGUgXCJkYXRhLXNuYW1lXCJcbi8vdGhlIGZvcm0gZWxlbWVudHMgc2hvdWxkIGJlIHdyYXBwZWQgaW4gYSBjbGFzcyBcImZvcm0tZWxlbWVudHNcIlxuLy90aGUgc3VjY2VzcyBtZXNzYWdlIHNob3VsZCBiZSB3cmFwcGVkIGluIGEgY2xhc3MgXCJzdWNjZXNzLW1lc3NhZ2VcIlxuLy90aGUgZmFpbCBtZXNzYWdlIHNob3VsZCBiZSB3cmFwcGVkIGluIGEgY2xhc3MgXCJmYWlsLW1lc3NhZ2VcIlxuLy9hZGQgZGF0YS1jbGVhciB0byB0aGUgZm9ybSBpZiB5b3Ugd2FudCB0byBoaWRlIGFsbCBmb3JtIGVsZW1lbnRzIG9uIHN1Ym1pdFxuXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpe1xuXG4gICQuZm4uaXNJblZpZXdwb3J0ID0gZnVuY3Rpb24oKSB7XG4gICAgdmFyIGVsZW1lbnRUb3AgPSAkKHRoaXMpLm9mZnNldCgpLnRvcDtcbiAgICB2YXIgZWxlbWVudEJvdHRvbSA9IGVsZW1lbnRUb3AgKyAkKHRoaXMpLm91dGVySGVpZ2h0KCk7XG4gICAgdmFyIHZpZXdwb3J0VG9wID0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpO1xuICAgIHZhciB2aWV3cG9ydEJvdHRvbSA9IHZpZXdwb3J0VG9wICsgJCh3aW5kb3cpLmhlaWdodCgpO1xuICAgIHJldHVybiBlbGVtZW50Qm90dG9tID4gdmlld3BvcnRUb3AgJiYgZWxlbWVudFRvcCA8IHZpZXdwb3J0Qm90dG9tO1xuICB9O1xuXG4gIGZ1bmN0aW9uIGdldFNjcm9sbGJhcldpZHRoKCkge1xuICAgIGNvbnN0IG91dGVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgb3V0ZXIuc3R5bGUudmlzaWJpbGl0eSA9ICdoaWRkZW4nO1xuICAgIG91dGVyLnN0eWxlLm92ZXJmbG93ID0gJ3Njcm9sbCc7XG4gICAgb3V0ZXIuc3R5bGUubXNPdmVyZmxvd1N0eWxlID0gJ3Njcm9sbGJhcic7XG4gICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChvdXRlcik7XG5cbiAgICBjb25zdCBpbm5lciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgIG91dGVyLmFwcGVuZENoaWxkKGlubmVyKTtcblxuICAgIGNvbnN0IHNjcm9sbGJhcldpZHRoID0gKG91dGVyLm9mZnNldFdpZHRoIC0gaW5uZXIub2Zmc2V0V2lkdGgpO1xuXG4gICAgb3V0ZXIucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChvdXRlcik7XG5cbiAgICByZXR1cm4gc2Nyb2xsYmFyV2lkdGg7XG4gIH1cbiAgJCgnI21vYmlsZS1uYXYtYnV0dG9uJykuY2xpY2soZnVuY3Rpb24oKSB7XG4gICAgdmFyIGN1clNjcm9sbGJhcldpZHRoID0gZ2V0U2Nyb2xsYmFyV2lkdGgoKTtcbiAgICBpZiAoJCh0aGlzKS5oYXNDbGFzcygnaXMtYWN0aXZlJykpIHtcbiAgICAgICQodGhpcykuY3NzKCdwYWRkaW5nLXJpZ2h0JywgJys9JyArIGN1clNjcm9sbGJhcldpZHRoICsgJ3B4Jyk7XG5cbiAgICAgIC8vIGhpZGUgc3VibmF2ICYgcmVzdG9yZSBkZWZhdWx0IGhlYWRlciB0aXRsZVxuICAgICAgaWYgKCQoJy5zdWJuYXYnKS5pcygnOnZpc2libGUnKSkge1xuICAgICAgICAvLyBoaWRlIHN1Ym5hdlxuICAgICAgICAkKCcuc3VibmF2Jykuc2xpZGVVcCg0MDAsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICQoJy5tb2JpbGUtbmF2JykucmVtb3ZlQ2xhc3MoJ3N1Ym5hdi1hY3RpdmUnKTtcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgICAvLyByZXN0b3JlIGRlZmF1bHQgaGVhZGVyIHRpdGxlXG4gICAgICAkKCcuc3VtbWl0LWxvZ28uZGVmYXVsdCcpLnNob3coKTtcbiAgICAgIC8vICQoJy5zdW1taXQtbG9nby5zdWJuYXYtdG9nZ2xlJykuaGlkZSgpO1xuICAgICAgJCgnLnN1bW1pdC1sb2dvLW5hdi13cmFwcGVyJykuaGlkZSgpO1xuICAgIH0gZWxzZSB7XG4gICAgICAkKHRoaXMpLmNzcygncGFkZGluZy1yaWdodCcsICctPScgKyBjdXJTY3JvbGxiYXJXaWR0aCArICdweCcpO1xuXG4gICAgICAvLyBpZiBvbiBhIHN1YiBwYWdlIHdpdGggc2libGluZ3Mgb3IgcGFnZSB3aXRoIGNoaWxkcmVuLCByZXN0b3JlIHN1Ym5hdlxuICAgICAgaWYgKCQoJy5zdWJuYXYtdG9nZ2xlJykubGVuZ3RoKSB7XG4gICAgICAgICQoJy5zdW1taXQtbG9nby5kZWZhdWx0JykuaGlkZSgpO1xuICAgICAgICAvLyAkKCcuc3VtbWl0LWxvZ28uc3VibmF2LXRvZ2dsZScpLnNob3coKTtcbiAgICAgICAgJCgnLnN1bW1pdC1sb2dvLW5hdi13cmFwcGVyJykuc2hvdygpO1xuICAgICAgfVxuICAgIH1cbiAgfSk7XG5cbiAgdmFyIGFkZEFuaW1hdGlvblRvUGFnZSA9IGZhbHNlO1xuICBpZiAoJCgnLmFuaW0tc3VidGxlLWxlZnQnKS5sZW5ndGgpIHtcbiAgICBhZGRBbmltYXRpb25Ub1BhZ2UgPSB0cnVlO1xuICAgICQoJy5hbmltLXN1YnRsZS1sZWZ0JykuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgIGlmICgkKHRoaXMpLmlzSW5WaWV3cG9ydCgpKSB7XG4gICAgICAgICQodGhpcykuZmluZCgnaW1nJykuYWRkQ2xhc3MoJ2FuaW1hdGUnKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHZhciBhZGRNb2JpbGVBbmltYXRpb25Ub1BhZ2UgPSBmYWxzZTtcbiAgaWYgKCQoJy5hbmltLXN1YnRsZS1sZWZ0LW1vYmlsZScpLmxlbmd0aCkge1xuICAgIGFkZE1vYmlsZUFuaW1hdGlvblRvUGFnZSA9IHRydWU7XG4gICAgJCgnLmFuaW0tc3VidGxlLWxlZnQtbW9iaWxlJykuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgIGlmICgkKHRoaXMpLmlzSW5WaWV3cG9ydCgpKSB7XG4gICAgICAgICQodGhpcykuZmluZCgnaW1nJykuYWRkQ2xhc3MoJ2FuaW1hdGUnKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHZhciBhZGRNYXJxdWVlQW5pbWF0aW9uVG9QYWdlID0gZmFsc2U7XG4gIGlmICgkKCcuYW5pbS1zdWJ0bGUtbGVmdC1tYXJxdWVlJykubGVuZ3RoKSB7XG4gICAgYWRkTWFycXVlZUFuaW1hdGlvblRvUGFnZSA9IHRydWU7XG4gICAgaWYgKCQoJy5hbmltLXN1YnRsZS1sZWZ0LW1hcnF1ZWUnKS5pc0luVmlld3BvcnQoKSkge1xuICAgICAgJCgnLmFuaW0tc3VidGxlLWxlZnQtbWFycXVlZScpLmFkZENsYXNzKCdhbmltYXRlJyk7XG4gICAgfVxuICB9XG5cbiAgdmFyIGFkZE1vYmlsZU1hcnF1ZWVBbmltYXRpb25Ub1BhZ2UgPSBmYWxzZTtcbiAgaWYgKCQoJy5hbmltLXN1YnRsZS1sZWZ0LW1hcnF1ZWUtbW9iaWxlJykubGVuZ3RoKSB7XG4gICAgYWRkTW9iaWxlTWFycXVlZUFuaW1hdGlvblRvUGFnZSA9IHRydWU7XG4gICAgaWYgKCQoJy5hbmltLXN1YnRsZS1sZWZ0LW1hcnF1ZWUtbW9iaWxlJykuaXNJblZpZXdwb3J0KCkpIHtcbiAgICAgICQoJy5hbmltLXN1YnRsZS1sZWZ0LW1hcnF1ZWUtbW9iaWxlJykuYWRkQ2xhc3MoJ2FuaW1hdGUnKTtcbiAgICB9XG4gIH1cblxuICAkKHdpbmRvdykub24oJ3Njcm9sbCcsIGZ1bmN0aW9uKCl7XG4gICAgaWYoJCh0aGlzKS5zY3JvbGxUb3AoKSA+IDIwKSB7XG4gICAgICAkKCcubmF2LXdyYXBwZXInKS5hZGRDbGFzcygnd2l0aC1iZycpO1xuICAgIH0gZWxzZSB7XG4gICAgICAkKCcubmF2LXdyYXBwZXInKS5yZW1vdmVDbGFzcygnd2l0aC1iZycpO1xuICAgIH1cblxuICAgIGlmIChhZGRBbmltYXRpb25Ub1BhZ2UpIHtcbiAgICAgICQoJy5hbmltLXN1YnRsZS1sZWZ0JykuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgICAgaWYgKCQodGhpcykuaXNJblZpZXdwb3J0KCkpIHtcbiAgICAgICAgICAkKHRoaXMpLmZpbmQoJ2ltZycpLmFkZENsYXNzKCdhbmltYXRlJyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgJCh0aGlzKS5maW5kKCdpbWcnKS5yZW1vdmVDbGFzcygnYW5pbWF0ZScpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBpZiAoYWRkTW9iaWxlQW5pbWF0aW9uVG9QYWdlKSB7XG4gICAgICAkKCcuYW5pbS1zdWJ0bGUtbGVmdC1tb2JpbGUnKS5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgICBpZiAoJCh0aGlzKS5pc0luVmlld3BvcnQoKSkge1xuICAgICAgICAgICQodGhpcykuZmluZCgnaW1nJykuYWRkQ2xhc3MoJ2FuaW1hdGUnKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAkKHRoaXMpLmZpbmQoJ2ltZycpLnJlbW92ZUNsYXNzKCdhbmltYXRlJyk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGlmIChhZGRNYXJxdWVlQW5pbWF0aW9uVG9QYWdlKSB7XG4gICAgICBpZiAoJCgnLmFuaW0tc3VidGxlLWxlZnQtbWFycXVlZScpLmlzSW5WaWV3cG9ydCgpKSB7XG4gICAgICAgICQoJy5hbmltLXN1YnRsZS1sZWZ0LW1hcnF1ZWUnKS5hZGRDbGFzcygnYW5pbWF0ZScpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgJCgnLmFuaW0tc3VidGxlLWxlZnQtbWFycXVlZScpLnJlbW92ZUNsYXNzKCdhbmltYXRlJyk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKGFkZE1vYmlsZU1hcnF1ZWVBbmltYXRpb25Ub1BhZ2UpIHtcbiAgICAgIGlmICgkKCcuYW5pbS1zdWJ0bGUtbGVmdC1tYXJxdWVlLW1vYmlsZScpLmlzSW5WaWV3cG9ydCgpKSB7XG4gICAgICAgICQoJy5hbmltLXN1YnRsZS1sZWZ0LW1hcnF1ZWUtbW9iaWxlJykuYWRkQ2xhc3MoJ2FuaW1hdGUnKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgICQoJy5hbmltLXN1YnRsZS1sZWZ0LW1hcnF1ZWUtbW9iaWxlJykucmVtb3ZlQ2xhc3MoJ2FuaW1hdGUnKTtcbiAgICAgIH1cbiAgICB9XG4gIH0pO1xuXG4gIGlmICgkKCcuc3BvbnNvcnMtbW9iaWxlJykubGVuZ3RoKSB7XG4gICAgJCgnLnNwb25zb3JzLW1vYmlsZSAuYWNjb3JkaW9uLXRpdGxlJykuY2xpY2soZnVuY3Rpb24oZSkge1xuICAgICAgaWYgKCQodGhpcykucGFyZW50KCkuaGFzQ2xhc3MoJ2lzLWFjdGl2ZScpKSB7XG4gICAgICAgICQoJyNzcG9uc29yLWFjY29yZGlvbicpLmFkZENsYXNzKCdpcy1hY3RpdmUnKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgICQoJyNzcG9uc29yLWFjY29yZGlvbicpLnJlbW92ZUNsYXNzKCdpcy1hY3RpdmUnKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIGlmICgkKCcubWkta2V5bm90ZS1zcGVha2VyLXJhbmRvbScpLmxlbmd0aCkge1xuICAgIHZhciBtaUtleVNwZWFrZXJzID0gW1xuICAgICAge1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgZmlyc3ROYW1lOiAnR2FyeScsXG4gICAgICAgIGxhc3ROYW1lOiAnVmF5bmVyY2h1aycsXG4gICAgICAgIGNvbXBhbnk6ICdWYXluZXJYJ1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgZmlyc3ROYW1lOiAnVHJveScsXG4gICAgICAgIGxhc3ROYW1lOiAnQnJvd24nLFxuICAgICAgICBjb21wYW55OiAnWnVtaWV6J1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgZmlyc3ROYW1lOiAnSnVsZXMnLFxuICAgICAgICBsYXN0TmFtZTogJ1BpZXJpJyxcbiAgICAgICAgY29tcGFueTogJ1RoZSBHcm9tbWV0J1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgZmlyc3ROYW1lOiAnR2lsbGlhbicsXG4gICAgICAgIGxhc3ROYW1lOiAnQ2FtcGJlbGwnLFxuICAgICAgICBjb21wYW55OiAnSFAgSW5jLidcbiAgICAgIH1cbiAgICBdO1xuICAgIHZhciByYW5kb21TcGVha2VySWQgPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiBNYXRoLmZsb29yKDQpKSArIDE7XG4gICAgdmFyIGtleVNwZWFrZXJTcmMgPSAkKCcubWkta2V5bm90ZS1zcGVha2VyLXJhbmRvbScpLmF0dHIoJ3NyYycpLnNsaWNlKDAsIC01KTtcbiAgICAkKCcubWkta2V5bm90ZS1zcGVha2VyLXJhbmRvbScpLmF0dHIoJ3NyYycsIGtleVNwZWFrZXJTcmMgKyByYW5kb21TcGVha2VySWQgKyAnLmpwZycpO1xuICAgICQoJy5taS1rZXlub3RlLXNwZWFrZXItcmFuZG9tLWZpcnN0bmFtZScpLnRleHQobWlLZXlTcGVha2Vyc1tyYW5kb21TcGVha2VySWRdLmZpcnN0TmFtZSk7XG4gICAgJCgnLm1pLWtleW5vdGUtc3BlYWtlci1yYW5kb20tbGFzdG5hbWUnKS50ZXh0KG1pS2V5U3BlYWtlcnNbcmFuZG9tU3BlYWtlcklkXS5sYXN0TmFtZSk7XG4gICAgJCgnLm1pLWtleW5vdGUtc3BlYWtlci1yYW5kb20tY29tcGFueScpLnRleHQobWlLZXlTcGVha2Vyc1tyYW5kb21TcGVha2VySWRdLmNvbXBhbnkpO1xuICB9XG5cbiAgaWYgKCQoJy52aWRlby1tb2RhbC10cmlnZ2VyJykubGVuZ3RoKSB7XG4gICAgdmFyIGNsb3NlQnRuID0ge1xuICAgICAgdG9wT2Zmc2V0OiAxNixcbiAgICAgIGxlZnRPZmZzZXQ6IDE1XG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIHJlc2V0Q2xvc2VCdG4oKSB7XG4gICAgICAvLyBhZGp1c3QgY2xvc2UtYnRuIHBvc2l0aW9uXG4gICAgICB2YXIgdmlkZW9FbFBvcyA9ICQoJy52aWRlby1jb250ZW50JykucG9zaXRpb24oKTtcbiAgICAgICQoJy52aWRlby1vdmVybGF5IC5jbG9zZS1idG4nKS5jc3MoJ3RvcCcsIHZpZGVvRWxQb3MudG9wIC0gY2xvc2VCdG4udG9wT2Zmc2V0KS5jc3MoJ2xlZnQnLCB2aWRlb0VsUG9zLmxlZnQgLSBjbG9zZUJ0bi5sZWZ0T2Zmc2V0ICsgJCgnLnZpZGVvLWNvbnRlbnQnKS53aWR0aCgpKTtcbiAgICB9XG5cbiAgICAkKHdpbmRvdykub24oJ3Jlc2l6ZScsIGZ1bmN0aW9uKCkge1xuICAgICAgaWYgKCQoJy52aWRlby1vdmVybGF5IC5jbG9zZS1idG4nKS5pcygnOnZpc2libGUnKSkge1xuICAgICAgICByZXNldENsb3NlQnRuKCk7XG4gICAgICB9XG4gICAgICBpZiAoJCgnLmRpc2FibGUtc2xpZGUtZWZmZWN0JykuY3NzKCd3aWR0aCcpID09ICcwJyB8fCAkKCcuZGlzYWJsZS1zbGlkZS1lZmZlY3QnKS5jc3MoJ3dpZHRoJykgPT0gJzBweCcpIHtcbiAgICAgICAgJCgnLnBhZ2UtaG9tZScpLnJlbW92ZUNsYXNzKCdkaXNhYmxlLWZpeGVkJyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAkKCcucGFnZS1ob21lJykuYWRkQ2xhc3MoJ2Rpc2FibGUtZml4ZWQnKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIGlmICgkKCcuZGlzYWJsZS1zbGlkZS1lZmZlY3QnKS5jc3MoJ3dpZHRoJykgPT0gJzAnIHx8ICQoJy5kaXNhYmxlLXNsaWRlLWVmZmVjdCcpLmNzcygnd2lkdGgnKSA9PSAnMHB4Jykge1xuICAgICAgJCgnLnBhZ2UtaG9tZScpLnJlbW92ZUNsYXNzKCdkaXNhYmxlLWZpeGVkJyk7XG4gICAgfSBlbHNlIHtcbiAgICAgICQoJy5wYWdlLWhvbWUnKS5hZGRDbGFzcygnZGlzYWJsZS1maXhlZCcpO1xuICAgIH1cblxuICAgICQoJy52aWRlby1tb2RhbC10cmlnZ2VyJykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSkge1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgdmFyIHZpZGVvRWwgPSAnLnZpZGVvLW92ZXJsYXlbZGF0YS1pZD0nICsgJCh0aGlzKS5hdHRyKCdkYXRhLXRhcmdldCcpICsgJ10nO1xuICAgICAgJCh2aWRlb0VsICsgJyBpZnJhbWUnKS5hdHRyKCdzcmMnLCAkKHZpZGVvRWwgKyAnIGlmcmFtZScpLmF0dHIoJ2RhdGEtc3JjJykpO1xuICAgICAgJCh2aWRlb0VsKS5zdG9wKCkuZmFkZUluKCk7XG4gICAgICByZXNldENsb3NlQnRuKCk7XG4gICAgfSk7XG5cbiAgICAkKCcudmlkZW8tb3ZlcmxheScpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpIHtcbiAgICAgIGlmICghJChlLnRhcmdldCkuaGFzQ2xhc3MoJ2Nsb3NlLWJ0bicpKSB7XG4gICAgICAgIHZhciB2aWRlb0VsID0gJy52aWRlby1vdmVybGF5W2RhdGEtaWQ9JyArICQodGhpcykuYXR0cignZGF0YS1pZCcpICsgJ10nO1xuICAgICAgICAkKHZpZGVvRWwgKyAnIGlmcmFtZScpLmF0dHIoJ3NyYycsICcnKTtcbiAgICAgICAgJCh0aGlzKS5zdG9wKCkuZmFkZU91dCgpO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgJCgnLnZpZGVvLW92ZXJsYXkgLmNsb3NlLWJ0bicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuICAgICAgJCgnLnZpZGVvLW92ZXJsYXknKS5zdG9wKCkuZmFkZU91dCgpO1xuICAgICAgdmFyIHZpZGVvRWwgPSAnLnZpZGVvLW92ZXJsYXlbZGF0YS1pZD0nICsgJCh0aGlzKS5wYXJlbnQoKS5wYXJlbnQoKS5hdHRyKCdkYXRhLWlkJykgKyAnXSc7XG4gICAgICAkKHZpZGVvRWwgKyAnIGlmcmFtZScpLmF0dHIoJ3NyYycsICcnKTtcbiAgICB9KTtcbiAgfVxuXG5cdCQoJ2Zvcm0uYWRvYmUtY2FtcGFpZ24nKS5lYWNoKGZ1bmN0aW9uKCl7XG5cdFx0cHJldmVudERlYWZhdWx0QmVoYXZpb3VyKCQodGhpcykpO1xuICB9KTtcbiAgXG4gICQuZm4uc3dpcGVEZXRlY3RvciA9IGZ1bmN0aW9uIChvcHRpb25zKSB7XG4gICAgLy8gU3RhdGVzOiAwIC0gbm8gc3dpcGUsIDEgLSBzd2lwZSBzdGFydGVkLCAyIC0gc3dpcGUgcmVsZWFzZWRcbiAgICB2YXIgc3dpcGVTdGF0ZSA9IDA7XG4gICAgLy8gQ29vcmRpbmF0ZXMgd2hlbiBzd2lwZSBzdGFydGVkXG4gICAgdmFyIHN0YXJ0WCA9IDA7XG4gICAgdmFyIHN0YXJ0WSA9IDA7XG4gICAgLy8gRGlzdGFuY2Ugb2Ygc3dpcGVcbiAgICB2YXIgcGl4ZWxPZmZzZXRYID0gMDtcbiAgICB2YXIgcGl4ZWxPZmZzZXRZID0gMDtcbiAgICAvLyBUYXJnZXQgZWxlbWVudCB3aGljaCBzaG91bGQgZGV0ZWN0IHN3aXBlcy5cbiAgICB2YXIgc3dpcGVUYXJnZXQgPSB0aGlzO1xuICAgIHZhciBkZWZhdWx0U2V0dGluZ3MgPSB7XG4gICAgICAvLyBBbW91bnQgb2YgcGl4ZWxzLCB3aGVuIHN3aXBlIGRvbid0IGNvdW50LlxuICAgICAgc3dpcGVUaHJlc2hvbGQ6IDcwLFxuICAgICAgLy8gRmxhZyB0aGF0IGluZGljYXRlcyB0aGF0IHBsdWdpbiBzaG91bGQgcmVhY3Qgb25seSBvbiB0b3VjaCBldmVudHMuXG4gICAgICAvLyBOb3Qgb24gbW91c2UgZXZlbnRzIHRvby5cbiAgICAgIHVzZU9ubHlUb3VjaDogZmFsc2VcbiAgICB9O1xuICAgIFxuICAgIC8vIEluaXRpYWxpemVyXG4gICAgKGZ1bmN0aW9uIGluaXQoKSB7XG4gICAgICBvcHRpb25zID0gJC5leHRlbmQoZGVmYXVsdFNldHRpbmdzLCBvcHRpb25zKTtcbiAgICAgIC8vIFN1cHBvcnQgdG91Y2ggYW5kIG1vdXNlIGFzIHdlbGwuXG4gICAgICBzd2lwZVRhcmdldC5vbignbW91c2Vkb3duIHRvdWNoc3RhcnQnLCBzd2lwZVN0YXJ0KTtcbiAgICAgICQoJ2h0bWwnKS5vbignbW91c2V1cCB0b3VjaGVuZCcsIHN3aXBlRW5kKTtcbiAgICAgICQoJ2h0bWwnKS5vbignbW91c2Vtb3ZlIHRvdWNobW92ZScsIHN3aXBpbmcpO1xuICAgIH0pKCk7XG4gICAgXG4gICAgZnVuY3Rpb24gc3dpcGVTdGFydChldmVudCkge1xuICAgICAgaWYgKG9wdGlvbnMudXNlT25seVRvdWNoICYmICFldmVudC5vcmlnaW5hbEV2ZW50LnRvdWNoZXMpXG4gICAgICAgIHJldHVybjtcbiAgICAgIFxuICAgICAgaWYgKGV2ZW50Lm9yaWdpbmFsRXZlbnQudG91Y2hlcylcbiAgICAgICAgZXZlbnQgPSBldmVudC5vcmlnaW5hbEV2ZW50LnRvdWNoZXNbMF07XG4gICAgICBcbiAgICAgIGlmIChzd2lwZVN0YXRlID09PSAwKSB7XG4gICAgICAgIHN3aXBlU3RhdGUgPSAxO1xuICAgICAgICBzdGFydFggPSBldmVudC5jbGllbnRYO1xuICAgICAgICBzdGFydFkgPSBldmVudC5jbGllbnRZO1xuICAgICAgfVxuICAgIH1cbiAgICBcbiAgICBmdW5jdGlvbiBzd2lwZUVuZChldmVudCkge1xuICAgICAgaWYgKHN3aXBlU3RhdGUgPT09IDIpIHtcbiAgICAgICAgc3dpcGVTdGF0ZSA9IDA7XG4gICAgICAgIFxuICAgICAgICBpZiAoTWF0aC5hYnMocGl4ZWxPZmZzZXRYKSA+IE1hdGguYWJzKHBpeGVsT2Zmc2V0WSkgJiZcbiAgICAgICAgICAgTWF0aC5hYnMocGl4ZWxPZmZzZXRYKSA+IG9wdGlvbnMuc3dpcGVUaHJlc2hvbGQpIHsgLy8gSG9yaXpvbnRhbCBTd2lwZVxuICAgICAgICAgIGlmIChwaXhlbE9mZnNldFggPCAwKSB7XG4gICAgICAgICAgICBzd2lwZVRhcmdldC50cmlnZ2VyKCQuRXZlbnQoJ3N3aXBlTGVmdC5zZCcpKTtcbiAgICAgICAgICAgIC8vY29uc29sZS5sb2coJ0xlZnQnKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgc3dpcGVUYXJnZXQudHJpZ2dlcigkLkV2ZW50KCdzd2lwZVJpZ2h0LnNkJykpO1xuICAgICAgICAgICAgLy9jb25zb2xlLmxvZygnUmlnaHQnKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSBpZiAoTWF0aC5hYnMocGl4ZWxPZmZzZXRZKSA+IG9wdGlvbnMuc3dpcGVUaHJlc2hvbGQpIHsgLy8gVmVydGljYWwgc3dpcGVcbiAgICAgICAgICBpZiAocGl4ZWxPZmZzZXRZIDwgMCkge1xuICAgICAgICAgICAgc3dpcGVUYXJnZXQudHJpZ2dlcigkLkV2ZW50KCdzd2lwZVVwLnNkJykpO1xuICAgICAgICAgICAgLy9jb25zb2xlLmxvZygnVXAnKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgc3dpcGVUYXJnZXQudHJpZ2dlcigkLkV2ZW50KCdzd2lwZURvd24uc2QnKSk7XG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKCdEb3duJyk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIFxuICAgIGZ1bmN0aW9uIHN3aXBpbmcoZXZlbnQpIHtcbiAgICAgIC8vIElmIHN3aXBlIGRvbid0IG9jY3VyaW5nLCBkbyBub3RoaW5nLlxuICAgICAgaWYgKHN3aXBlU3RhdGUgIT09IDEpIHJldHVybjtcbiAgICAgIFxuICAgICAgaWYgKGV2ZW50Lm9yaWdpbmFsRXZlbnQudG91Y2hlcykge1xuICAgICAgICBldmVudCA9IGV2ZW50Lm9yaWdpbmFsRXZlbnQudG91Y2hlc1swXTtcbiAgICAgIH1cbiAgICAgIFxuICAgICAgdmFyIHN3aXBlT2Zmc2V0WCA9IGV2ZW50LmNsaWVudFggLSBzdGFydFg7XG4gICAgICB2YXIgc3dpcGVPZmZzZXRZID0gZXZlbnQuY2xpZW50WSAtIHN0YXJ0WTtcbiAgICAgIFxuICAgICAgaWYgKChNYXRoLmFicyhzd2lwZU9mZnNldFgpID4gb3B0aW9ucy5zd2lwZVRocmVzaG9sZCkgfHxcbiAgICAgICAgICAoTWF0aC5hYnMoc3dpcGVPZmZzZXRZKSA+IG9wdGlvbnMuc3dpcGVUaHJlc2hvbGQpKSB7XG4gICAgICAgIHN3aXBlU3RhdGUgPSAyO1xuICAgICAgICBwaXhlbE9mZnNldFggPSBzd2lwZU9mZnNldFg7XG4gICAgICAgIHBpeGVsT2Zmc2V0WSA9IHN3aXBlT2Zmc2V0WTtcbiAgICAgICAgLy9jb25zb2xlLmxvZyhwaXhlbE9mZnNldFgpO1xuICAgICAgfVxuICAgIH1cbiAgICBcbiAgICByZXR1cm4gc3dpcGVUYXJnZXQ7IC8vIFJldHVybiBlbGVtZW50IGF2YWlsYWJsZSBmb3IgY2hhaW5pbmcuXG4gIH1cblxuICBpZiAoJCgnLmhvbWVwYWdlLWNvbnRlbnQnKS5sZW5ndGgpIHtcbiAgICAkKCdib2R5Jykuc3dpcGVEZXRlY3RvcigpO1xuICAgICQoJ2JvZHknKS5vbignc3dpcGVVcC5zZCcsIGZ1bmN0aW9uKCkge1xuICAgICAgaWYgKCEkKCcucGFnZS1ob21lJykuaGFzQ2xhc3MoJ2Rpc2FibGUtZml4ZWQnKSkge1xuICAgICAgICBuZXh0Q29udGVudCgpO1xuICAgICAgfVxuICAgIH0pO1xuICAgICQoJ2JvZHknKS5vbignc3dpcGVEb3duLnNkJywgZnVuY3Rpb24oKSB7XG4gICAgICBpZiAoISQoJy5wYWdlLWhvbWUnKS5oYXNDbGFzcygnZGlzYWJsZS1maXhlZCcpKSB7XG4gICAgICAgIHByZXZDb250ZW50KCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICAvLyBpbml0IEFPU1xuICAvL0FPUy5pbml0KCk7XG5cbiAgLy8gaGVhZGVyIG5hdiBzbGlkZSBlZmZlY3RzXG4gICQoJy5oZWFkZXItbmF2ID4gLmlzLWRyb3Bkb3duLXN1Ym1lbnUtcGFyZW50JykuaG92ZXIoZnVuY3Rpb24oKSB7XG4gICAgJCh0aGlzKS5jaGlsZHJlbigndWwuc3VibWVudScpLnN0b3AoKS5zbGlkZURvd24oMjUwKTtcbiAgfSwgZnVuY3Rpb24oKSB7XG4gICAgJCh0aGlzKS5jaGlsZHJlbigndWwuc3VibWVudScpLnN0b3AoKS5zbGlkZVVwKDI1MCk7XG4gIH0pO1xuXG4gIC8vIGZvb3RlciBtYWlsaW5nbGlzdCBzaWdudXAgYnV0dG9uIGFuaW1hdGlvbiAobW9ycGggdG8gaW5wdXQpXG4gICQoJy5idXR0b24tbW9ycGgtZm9vdGVyJykuY2xpY2soZnVuY3Rpb24oKSB7XG4gICAgaWYgKCEkKHRoaXMpLmhhc0NsYXNzKCdhY3RpdmUnKSkge1xuICAgICAgJCh0aGlzKS5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgICAkKHRoaXMpLmF0dHIoJ3BsYWNlaG9sZGVyJywgJ0VtYWlsJyk7XG4gICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAkKCcuYnV0dG9uLW1vcnBoLWZvb3Rlci1zdWJtaXQnKS5mYWRlSW4oKTtcbiAgICAgIH0sIDIwMCk7XG4gICAgfVxuICB9KTtcblxuICAvLyBtYWlsaW5nbGlzdCBzaWdudXAgc3VibWl0XG4gIHZhciBsYW5ndWFnZXNDbGFzc2VzID0gWydkZWZhdWx0JywgJ2JsdWUnLCAnb3JhbmdlJywgJ2dyZWVuJywgJ3BpbmsnLCAneWVsbG93J107XG4gIHZhciBsYW5ndWFnZXNUZXh0ID0gWydUaGFuayB5b3UnLCAn44GC44KK44GM44Go44GG44GU44GW44GE44G+44GX44GfJywgJ9Ch0L/QsNGB0LjQsdC+JywgJ0dyYWNpYXMnLCAnSmUgdm91cyByZW1lcmNpZScsICfosKLosKInXTtcbiAgdmFyIGxhbmd1YWdlU3RlcCA9IDE7XG4gICQoJy5idXR0b24tbW9ycGgtZm9vdGVyLXN1Ym1pdCcpLmNsaWNrKGZ1bmN0aW9uKGUpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICBpZiAoJCgnLmJ1dHRvbi1tb3JwaC1mb290ZXInKS52YWwoKSA9PSAnJykge1xuICAgICAgYWxlcnQoJ1BsZWFzZSBlbnRlciB5b3VyIGVtYWlsIGFkZHJlc3MuJyk7XG4gICAgfSBlbHNlIHtcblxuICAgICAgdmFyIGZvcm0gPSAkKCcubWFpbGluZ2xpc3Qtd3JhcHBlcicpO1xuICAgICAgdmFyIHVzZXJFbWFpbCA9IHsgZW1haWw6ICQoJy5idXR0b24tbW9ycGgtZm9vdGVyJykudmFsKCkgfTtcbiAgICAgIHZhciBzbmFtZSA9IHsgc25hbWU6ICQoZm9ybSkuYXR0cignZGF0YS1zbmFtZScpIH07XG4gICAgICB2YXIgY24gPSAkKCcubWFpbGluZ2xpc3Qtd3JhcHBlcicpLmZpbmQoJy5sZWdhbCcpO1xuICAgICAgdmFyIGNvbnNlbnRfbm90aWNlID0geyBjb25zZW50X25vdGljZTogY24uaHRtbCgpIH07XG4gICAgICB2YXIgY3VycmVudF91cmw9IHsgJ2N1cnJlbnRfdXJsJzogd2luZG93LmxvY2F0aW9uLmhyZWYgfTtcbiAgICAgIC8vIGN1cnJlbnRfdXJsWydjdXJyZW50X3VybCddID0gd2luZG93LmxvY2F0aW9uLmhyZWZcbiAgICAgIHZhciBjb21wbGV0ZUpzb24gPSAkLmV4dGVuZCggdXNlckVtYWlsLCBzbmFtZSwgY29uc2VudF9ub3RpY2UsIGN1cnJlbnRfdXJsICk7XG4gICAgICB2YXIgc3RyaW5nSnNvbiA9IEpTT04uc3RyaW5naWZ5KGNvbXBsZXRlSnNvbik7XG5cbiAgICAgICQuYWpheCh7XG4gICAgICAgIHR5cGU6IFwiUE9TVFwiLFxuICAgICAgICB1cmw6IFwiaHR0cHM6Ly93d3cuYWRvYmUuY29tL2FwaTIvc3Vic2NyaWJlX3YxXCIsXG4gICAgICAgIGRhdGE6IHN0cmluZ0pzb24sXG4gICAgICAgIGNvbnRlbnRUeXBlOlwiYXBwbGljYXRpb24vanNvbjsgY2hhcnNldD11dGYtOFwiLFxuICAgICAgICBkYXRhVHlwZTogXCJqc29uXCJcbiAgICAgIH0pLmRvbmUoZnVuY3Rpb24oZGF0YSkge1xuICAgICAgICAkKCcuYnV0dG9uLW1vcnBoLWZvb3RlcicpLnZhbCgnJyk7XG4gICAgICAgIC8vIHN1Y2Nlc3MgLSBtb3JwaCBpbnRvIFwiVGhhbmsgeW91XCIgYnV0dG9uXG4gICAgICAgICQoJy5idXR0b24tbW9ycGgtZm9vdGVyLXN1Ym1pdCcpLmZhZGVPdXQoKTtcbiAgICAgICAgJCgnLmJ1dHRvbi1tb3JwaC1mb290ZXInKS5hZGRDbGFzcygnc2lnbnVwLXN1Y2Nlc3MnKTtcbiAgICAgICAgJCgnLmJ1dHRvbi1tb3JwaC1mb290ZXInKS5hdHRyKCdwbGFjZWhvbGRlcicsICdUaGFuayB5b3UnKTtcblxuICAgICAgICAvLyBzdGFydCB0ZXh0ICYgY29sb3VyIHJvdGF0aW9uXG4gICAgICAgIHNldEludGVydmFsKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICQoJy5idXR0b24tbW9ycGgtZm9vdGVyJykucmVtb3ZlQ2xhc3MoJ2JsdWUgb3JhbmdlIGdyZWVuIHBpbmsgeWVsbG93JykuYWRkQ2xhc3MoJ2ZhZGUtb3V0Jyk7XG4gICAgICAgICAgJCgnLmJ1dHRvbi1tb3JwaC1mb290ZXInKS5hdHRyKCdwbGFjZWhvbGRlcicsIGxhbmd1YWdlc1RleHRbbGFuZ3VhZ2VTdGVwXSkucmVtb3ZlQ2xhc3MoJ2ZhZGUtb3V0Jyk7XG4gICAgICAgICAgJCgnLmJ1dHRvbi1tb3JwaC1mb290ZXInKS5hZGRDbGFzcyhsYW5ndWFnZXNDbGFzc2VzW2xhbmd1YWdlU3RlcF0pO1xuICAgICAgICAgIGxhbmd1YWdlU3RlcCsrO1xuICAgICAgICAgIGlmIChsYW5ndWFnZVN0ZXAgPj0gbGFuZ3VhZ2VzQ2xhc3Nlcy5sZW5ndGgpIGxhbmd1YWdlU3RlcCA9IDA7XG4gICAgICAgIH0sIDMwMDApO1xuXG4gICAgICAgIC8vIGV2ZW50IHRyYWNrc1xuICAgICAgICBmYnEoJ3RyYWNrJywgJ0xlYWQnKTtcbiAgICAgICAgJCgnLmZvb3Rlci13cmFwcGVyJykuYXBwZW5kKCc8aW1nIGhlaWdodD1cIjFcIiB3aWR0aD1cIjFcIiBzdHlsZT1cImRpc3BsYXk6bm9uZTtcIiBhbHQ9XCJcIiBzcmM9XCJodHRwczovL3B4LmFkcy5saW5rZWRpbi5jb20vY29sbGVjdC8/cGlkPTQ0NzYzNiZjb252ZXJzaW9uSWQ9MTM3MTA1MiZmbXQ9Z2lmXCIgLz4nKTtcbiAgICAgICAgZ3RhZ19yZXBvcnRfY29udmVyc2lvbigpO1xuICAgICAgICB0d3R0ci5jb252ZXJzaW9uLnRyYWNrUGlkKCdvMmZ6dScsIHsgdHdfc2FsZV9hbW91bnQ6IDAsIHR3X29yZGVyX3F1YW50aXR5OiAwIH0pO1xuICAgICAgfSkuZmFpbChmdW5jdGlvbihkYXRhKSB7XG4gICAgICAgIGFsZXJ0KCdXZSBjb3VsZCBub3QgYWRkIHlvdSB0byBvdXIgbWFpbGluZyBsaXN0LiBQbGVhc2UgdHJ5IGFnYWluIGxhdGVyLicpO1xuXG4gICAgICAgICQoJy5idXR0b24tbW9ycGgtZm9vdGVyJykudmFsKCcnKTtcbiAgICAgICAgJCgnLmJ1dHRvbi1tb3JwaC1mb290ZXItc3VibWl0JykuaGlkZSgpO1xuICAgICAgICAkKCcuYnV0dG9uLW1vcnBoLWZvb3RlcicpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgJCgnLmJ1dHRvbi1tb3JwaC1mb290ZXInKS5hdHRyKCdwbGFjZWhvbGRlcicsICdTaWduIHVwJyk7XG4gICAgICB9KTtcblxuICAgIH1cbiAgfSk7XG5cbiAgLy8gdGVtcG9yYXJ5IG1haWxpbmdsaXN0IHNpZ251cCBidXR0b24gYW5pbWF0aW9uIHJlc2V0XG4gICQoJy5idXR0b24tbW9ycGgtZm9vdGVyLXJlc2V0JykuY2xpY2soZnVuY3Rpb24oKSB7XG4gICAgJCgnLmJ1dHRvbi1tb3JwaC1mb290ZXInKS5yZW1vdmVDbGFzcygnYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ3NpZ251cC1zdWNjZXNzJyk7XG4gICAgJCgnLmJ1dHRvbi1tb3JwaC1mb290ZXInKS5hdHRyKCdwbGFjZWhvbGRlcicsICdTaWduIHVwJyk7XG4gICAgJCgnLmJ1dHRvbi1tb3JwaC1mb290ZXItc3VibWl0JykuaGlkZSgpO1xuICB9KTtcblxuICAvLyBtb2JpbGUgbmF2IGNvbG91ciBjaGFuZ2VzIG9uIHRvZ2dsZSAmIGFkZCBjb250YWluZXIgY2xhc3Mgb24gcGFnZSBsb2FkIGlmIG5lZWRlZFxuICBpZiAoJCgnLmFjY29yZGlvbi1pdGVtLmlzLWFjdGl2ZScpLmxlbmd0aCkge1xuICAgICQoJy5hY2NvcmRpb24tY29udGFpbmVyJykuYWRkQ2xhc3MoJ2hhcy1hY3RpdmUtbGlua3MnKTtcbiAgfVxuICAkKCcubmF2LWxpbmsnKS5jbGljayhmdW5jdGlvbihlKSB7XG4gICAgdmFyIGFjdGl2ZVRvcExpbmtzID0gJCgnLmFjY29yZGlvbi1pdGVtLmlzLWFjdGl2ZScpLmxlbmd0aDtcbiAgICBpZiAoYWN0aXZlVG9wTGlua3MpIHtcbiAgICAgICQoJy5hY2NvcmRpb24tY29udGFpbmVyJykuYWRkQ2xhc3MoJ2hhcy1hY3RpdmUtbGlua3MnKTtcbiAgICAgIGlmICghJCh0aGlzKS5wYXJlbnQoKS5wcmV2KCkuaGFzQ2xhc3MoJ2lzLWFjdGl2ZScpKSB7XG4gICAgICAgICQodGhpcykucGFyZW50KCkucHJldigpLmFkZENsYXNzKCdpcy1hY3RpdmUnKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgICQodGhpcykucGFyZW50KCkucHJldigpLnJlbW92ZUNsYXNzKCdpcy1hY3RpdmUnKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgJCgnLmFjY29yZGlvbi1jb250YWluZXInKS5yZW1vdmVDbGFzcygnaGFzLWFjdGl2ZS1saW5rcycpO1xuICAgICAgJCgnLnRvcC1sZXZlbC1saW5rLmlzLWFjdGl2ZScpLnJlbW92ZUNsYXNzKCdpcy1hY3RpdmUnKTtcbiAgICB9XG4gIH0pO1xuXG4gIC8vIHN1Ym5hdiB0b2dnbGVcbiAgJCgnLnN1Ym5hdi10b2dnbGUnKS5jbGljayhmdW5jdGlvbihlKSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgaWYgKCQoJy5zdWJuYXYnKS5pcygnOnZpc2libGUnKSkge1xuICAgICAgLy8gaGlkZSBzdWJuYXZcbiAgICAgICQoJy5zdWJuYXYnKS5zbGlkZVVwKDQwMCwgZnVuY3Rpb24oKSB7XG4gICAgICAgICQoJy5tb2JpbGUtbmF2JykucmVtb3ZlQ2xhc3MoJ3N1Ym5hdi1hY3RpdmUnKTtcbiAgICAgIH0pOyAgICAgIFxuICAgIH0gZWxzZSB7XG4gICAgICAvLyBzaG93IHN1Ym5hdlxuICAgICAgJCgnLm1vYmlsZS1uYXYnKS5hZGRDbGFzcygnc3VibmF2LWFjdGl2ZScpO1xuICAgICAgJCgnLnN1Ym5hdicpLnNsaWRlRG93bigpO1xuXG4gICAgICBpZiAoISQoJy5uYXYtd3JhcHBlcicpLmhhc0NsYXNzKCd3aXRoLWJnJykpIHtcbiAgICAgICAgJCgnLm5hdi13cmFwcGVyJykuYWRkQ2xhc3MoJ3dpdGgtYmcnKTtcbiAgICAgIH1cbiAgICB9XG4gIH0pO1xuXG4gIC8vIGhvbWUgcGFnZSBjb250ZW50XG4gIHZhciBjdXJDb250ZW50SWQgPSAxO1xuICB2YXIgbWF4Q29udGVudElkID0gJCgnLmhvbWVwYWdlLWNvbnRlbnQnKS5sZW5ndGg7XG5cbiAgJCgnLnBhZ2UtaW5kaWNhdG9yLWN1cicpLnRleHQoY3VyQ29udGVudElkKTtcbiAgJCgnLnBhZ2UtaW5kaWNhdG9yLW1heCcpLnRleHQobWF4Q29udGVudElkKTtcblxuICAvLyBob21lIHBhZ2U6IGNvbnRlbnQgY29udHJvbHNcbiAgZm9yKHZhciBpID0gMTsgaSA8PSBtYXhDb250ZW50SWQ7IGkrKykge1xuICAgIHZhciBhY3RpdmVDbGFzcyA9IChpID09IGN1ckNvbnRlbnRJZCkgPyAnYWN0aXZlJyA6ICcnO1xuICAgICQoJy5ob21lcGFnZS1jb250cm9scy13cmFwcGVyIC5wYWdlLWNvbnRyb2xzJykuYXBwZW5kKCc8YSBocmVmPVwiI1wiIGNsYXNzPVwic2xpZGUnICsgaSArICcgJyArIGFjdGl2ZUNsYXNzICsgJ1wiPjxpbWcgY2xhc3M9XCJjaXJjbGUtYWN0aXZlXCIgc3JjPVwiL25hL19hc3NldHMvaW1hZ2VzL2hvbWVwYWdlL2NpcmNsZS1hY3RpdmUucG5nXCIgLz48aW1nIGNsYXNzPVwiY2lyY2xlLWluYWN0aXZlXCIgc3JjPVwiL25hL19hc3NldHMvaW1hZ2VzL2hvbWVwYWdlL2NpcmNsZS1pbmFjdGl2ZS5wbmdcIiAvPjwvYT4nKTtcbiAgfVxuXG4gIC8vIGhvbWUgcGFnZTogZ28gdG8gc2VsZWN0ZWQgY29udGVudCBzbGlkZVxuICAkKCcuaG9tZXBhZ2UtY29udHJvbHMtd3JhcHBlciAucGFnZS1jb250cm9scyBhJykuY2xpY2soZnVuY3Rpb24oZSkge1xuICAgIGUucHJldmVudERlZmF1bHQoKTtcblxuICAgIHZhciBzZWxlY3RlZENvbnRlbnRJZCA9IHBhcnNlSW50KCQodGhpcykuYXR0cignY2xhc3MnKS5yZXBsYWNlKCdzbGlkZScsICcnKSwgMTApO1xuXG4gICAgaWYgKHNlbGVjdGVkQ29udGVudElkICE9IGN1ckNvbnRlbnRJZCkge1xuICAgICAgJCgnLmhvbWVwYWdlLWNvbnRlbnQuaHBjJyArIGN1ckNvbnRlbnRJZCkuc3RvcCgpLnNsaWRlVXAoMTAwMCk7XG4gICAgICBjdXJDb250ZW50SWQgPSBzZWxlY3RlZENvbnRlbnRJZDtcbiAgICAgICQoJy5wYWdlLWluZGljYXRvci1jdXInKS50ZXh0KGN1ckNvbnRlbnRJZCk7XG4gICAgICAkKCcuaG9tZXBhZ2UtY29udHJvbHMtd3JhcHBlciAucGFnZS1jb250cm9scyBhLmFjdGl2ZScpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICQodGhpcykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgJCgnLmhvbWVwYWdlLWNvbnRlbnQuaHBjJyArIGN1ckNvbnRlbnRJZCkuc3RvcCgpLnNsaWRlRG93bigxMDAwKTtcbiAgICB9XG4gIH0pO1xuXG4gIGZ1bmN0aW9uIHByZXZDb250ZW50KCkge1xuICAgIGlmIChjdXJDb250ZW50SWQgPiAxKSB7XG4gICAgICAkKCcuaG9tZXBhZ2UtY29udGVudC5ocGMnICsgY3VyQ29udGVudElkKS5zdG9wKCkuc2xpZGVVcCgxMDAwKTtcbiAgICAgIGN1ckNvbnRlbnRJZC0tO1xuICAgICAgJCgnLnBhZ2UtaW5kaWNhdG9yLWN1cicpLnRleHQoY3VyQ29udGVudElkKTtcbiAgICAgICQoJy5ob21lcGFnZS1jb250cm9scy13cmFwcGVyIC5wYWdlLWNvbnRyb2xzIGEuYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgJCgnLmhvbWVwYWdlLWNvbnRyb2xzLXdyYXBwZXIgLnBhZ2UtY29udHJvbHMgYS5zbGlkZScgKyBjdXJDb250ZW50SWQpLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgICQoJy5ob21lcGFnZS1jb250ZW50LmhwYycgKyBjdXJDb250ZW50SWQpLnN0b3AoKS5zbGlkZURvd24oMTAwMCk7XG4gICAgfVxuICB9XG5cbiAgLy8gaG9tZSBwYWdlOiBnbyB0byBwcmV2aW91cyBjb250ZW50XG4gICQoJy5ob21lcGFnZS1jb250cm9scy13cmFwcGVyIC5wYWdlLWFycm93cyAucHJldicpLmNsaWNrKGZ1bmN0aW9uKGUpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgcHJldkNvbnRlbnQoKTtcbiAgfSk7XG5cbiAgZnVuY3Rpb24gbmV4dENvbnRlbnQoKSB7XG4gICAgaWYgKGN1ckNvbnRlbnRJZCA8IG1heENvbnRlbnRJZCkge1xuICAgICAgJCgnLmhvbWVwYWdlLWNvbnRlbnQuaHBjJyArIGN1ckNvbnRlbnRJZCkuc3RvcCgpLnNsaWRlVXAoMTAwMCk7XG4gICAgICBjdXJDb250ZW50SWQrKztcbiAgICAgICQoJy5wYWdlLWluZGljYXRvci1jdXInKS50ZXh0KGN1ckNvbnRlbnRJZCk7XG4gICAgICAkKCcuaG9tZXBhZ2UtY29udHJvbHMtd3JhcHBlciAucGFnZS1jb250cm9scyBhLmFjdGl2ZScpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICQoJy5ob21lcGFnZS1jb250cm9scy13cmFwcGVyIC5wYWdlLWNvbnRyb2xzIGEuc2xpZGUnICsgY3VyQ29udGVudElkKS5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgICAkKCcuaG9tZXBhZ2UtY29udGVudC5ocGMnICsgY3VyQ29udGVudElkKS5zdG9wKCkuc2xpZGVEb3duKDEwMDApO1xuICAgIH1cbiAgfVxuXG4gIC8vIGhvbWUgcGFnZTogZ28gdG8gbmV4dCBjb250ZW50XG4gICQoJy5ob21lcGFnZS1jb250cm9scy13cmFwcGVyIC5wYWdlLWFycm93cyAubmV4dCcpLmNsaWNrKGZ1bmN0aW9uKGUpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgbmV4dENvbnRlbnQoKTtcbiAgfSk7XG5cbiAgaWYgKCQoJy5ob21lcGFnZS1jb250cm9scy13cmFwcGVyJykubGVuZ3RoKSB7XG4gICAgJChkb2N1bWVudCkua2V5ZG93bihmdW5jdGlvbihlKSB7XG4gICAgICBzd2l0Y2goZS53aGljaCkge1xuICAgICAgICAgIGNhc2UgMzg6IC8vIHVwXG4gICAgICAgICAgICBpZiAoZGlmZl9zZWNvbmRzKCkgPiAxMjAwICYmICEkKCcucGFnZS1ob21lJykuaGFzQ2xhc3MoJ2Rpc2FibGUtZml4ZWQnKSkge1xuICAgICAgICAgICAgICBsYXN0U2Nyb2xsID0gbmV3IERhdGUoKTtcbiAgICAgICAgICAgICAgcHJldkNvbnRlbnQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBicmVhaztcbiAgXG4gICAgICAgICAgY2FzZSA0MDogLy8gZG93blxuICAgICAgICAgICAgaWYgKGRpZmZfc2Vjb25kcygpID4gMTIwMCAmJiAhJCgnLnBhZ2UtaG9tZScpLmhhc0NsYXNzKCdkaXNhYmxlLWZpeGVkJykpIHtcbiAgICAgICAgICAgICAgbGFzdFNjcm9sbCA9IG5ldyBEYXRlKCk7XG4gICAgICAgICAgICAgIG5leHRDb250ZW50KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgYnJlYWs7XG4gIFxuICAgICAgICAgIGRlZmF1bHQ6IHJldHVybjsgLy8gZXhpdCB0aGlzIGhhbmRsZXIgZm9yIG90aGVyIGtleXNcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIC8vIGhvbWUgcGFnZTogc2Nyb2xsIHRvIGNvbnRlbnRcbiAgdmFyIGxhc3RTY3JvbGwgPSBuZXcgRGF0ZSgpO1xuXG4gIGZ1bmN0aW9uIGRpZmZfc2Vjb25kcygpIHtcbiAgICB2YXIgZGF0ZU5vdyA9IG5ldyBEYXRlKCk7XG4gICAgdmFyIHRpbWVEaWZmZXJlbmNlID0gKGRhdGVOb3cuZ2V0VGltZSgpIC0gbGFzdFNjcm9sbC5nZXRUaW1lKCkpO1xuICAgIHJldHVybiBNYXRoLmFicyhNYXRoLnJvdW5kKHRpbWVEaWZmZXJlbmNlKSk7XG4gIH1cblxuICAkKGRvY3VtZW50KS5vbigndHJpZ2dlclNjcm9sbCcsIGZ1bmN0aW9uKGUsIHNjcm9sbERpcmVjdGlvbikge1xuICAgIGlmICgkKCcucGFnZS1ob21lJykuaGFzQ2xhc3MoJ2Rpc2FibGUtZml4ZWQnKSkge1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIGlmIChkaWZmX3NlY29uZHMoKSA+IDEyMDApIHtcbiAgICAgIGxhc3RTY3JvbGwgPSBuZXcgRGF0ZSgpO1xuICAgICAgaWYgKHNjcm9sbERpcmVjdGlvbiA9PSAxKSB7XG4gICAgICAgIC8vIHNjcm9sbCB1cCwgZ28gdG8gcHJldmlvdXMgY29udGVudFxuICAgICAgICBpZiAoY3VyQ29udGVudElkID4gMSkge1xuICAgICAgICAgICQoJy5ob21lcGFnZS1jb250ZW50LmhwYycgKyBjdXJDb250ZW50SWQpLnN0b3AoKS5zbGlkZVVwKDEwMDApO1xuICAgICAgICAgIGN1ckNvbnRlbnRJZC0tO1xuICAgICAgICAgICQoJy5wYWdlLWluZGljYXRvci1jdXInKS50ZXh0KGN1ckNvbnRlbnRJZCk7XG4gICAgICAgICAgJCgnLmhvbWVwYWdlLWNvbnRyb2xzLXdyYXBwZXIgLnBhZ2UtY29udHJvbHMgYS5hY3RpdmUnKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgJCgnLmhvbWVwYWdlLWNvbnRyb2xzLXdyYXBwZXIgLnBhZ2UtY29udHJvbHMgYS5zbGlkZScgKyBjdXJDb250ZW50SWQpLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAkKCcuaG9tZXBhZ2UtY29udGVudC5ocGMnICsgY3VyQ29udGVudElkKS5zdG9wKCkuc2xpZGVEb3duKDEwMDApO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAvLyBzY3JvbGwgZG93biwgZ28gdG8gbmV4dCBjb250ZW50XG4gICAgICAgIGlmIChjdXJDb250ZW50SWQgPCBtYXhDb250ZW50SWQpIHtcbiAgICAgICAgICAkKCcuaG9tZXBhZ2UtY29udGVudC5ocGMnICsgY3VyQ29udGVudElkKS5zdG9wKCkuc2xpZGVVcCgxMDAwKTtcbiAgICAgICAgICBjdXJDb250ZW50SWQrKztcbiAgICAgICAgICAkKCcucGFnZS1pbmRpY2F0b3ItY3VyJykudGV4dChjdXJDb250ZW50SWQpO1xuICAgICAgICAgICQoJy5ob21lcGFnZS1jb250cm9scy13cmFwcGVyIC5wYWdlLWNvbnRyb2xzIGEuYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICQoJy5ob21lcGFnZS1jb250cm9scy13cmFwcGVyIC5wYWdlLWNvbnRyb2xzIGEuc2xpZGUnICsgY3VyQ29udGVudElkKS5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgJCgnLmhvbWVwYWdlLWNvbnRlbnQuaHBjJyArIGN1ckNvbnRlbnRJZCkuc3RvcCgpLnNsaWRlRG93bigxMDAwKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfSk7XG5cbiAgdmFyIHN1cHBvcnRzV2hlZWwgPSBmYWxzZTtcbiAgZnVuY3Rpb24gd2hlZWwoZSkge1xuICAgIGlmIChlLnR5cGUgPT0gJ3doZWVsJykgc3VwcG9ydHNXaGVlbCA9IHRydWU7XG4gICAgZWxzZSBpZiAoc3VwcG9ydHNXaGVlbCkgcmV0dXJuO1xuICAgICQoZG9jdW1lbnQpLnRyaWdnZXIoXCJ0cmlnZ2VyU2Nyb2xsXCIsKChlLmRldGFpbDwwIHx8IGUud2hlZWxEZWx0YT4wIHx8IGUuZGVsdGFZIDwgMCB8fCBlLmRlbHRhWCA8IDApID8gMSA6IC0xKSk7XG4gIH07XG5cbiAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignd2hlZWwnLCB3aGVlbCwge3Bhc3NpdmU6IGZhbHNlfSwgZmFsc2UpO1xuICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZXdoZWVsJywgd2hlZWwsIHtwYXNzaXZlOiBmYWxzZX0sIGZhbHNlKTtcbiAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NTW91c2VTY3JvbGwnLCB3aGVlbCwge3Bhc3NpdmU6IGZhbHNlfSwgZmFsc2UpO1xuXG4vKlxuICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCd3aGVlbCcsIF8uZGVib3VuY2Uod2hlZWwsIDI1MCwgeyBsZWFkaW5nOiB0cnVlIH0pLCB7cGFzc2l2ZTogZmFsc2V9LCBmYWxzZSk7XG4gIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNld2hlZWwnLCBfLmRlYm91bmNlKHdoZWVsLCAyNTAsIHsgbGVhZGluZzogdHJ1ZSB9KSwge3Bhc3NpdmU6IGZhbHNlfSwgZmFsc2UpO1xuICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Nb3VzZVNjcm9sbCcsIF8uZGVib3VuY2Uod2hlZWwsIDI1MCwgeyBsZWFkaW5nOiB0cnVlIH0pLCB7cGFzc2l2ZTogZmFsc2V9LCBmYWxzZSk7XG4qL1xuXG59KTtcblxuZnVuY3Rpb24gcHJldmVudERlYWZhdWx0QmVoYXZpb3VyKCRmb3JtKXtcblx0JGZvcm0uYmluZCgnc3VibWl0JywgZnVuY3Rpb24oZXZlbnQpe1xuXHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdH0pO1xufVxuXG5cbiQoJ2Zvcm0uYWRvYmUtY2FtcGFpZ24nKS5vbihcImZvcm12YWxpZC56Zi5hYmlkZVwiLCBmdW5jdGlvbihldmVudCxmcm0pIHtcblx0XHR2YXIgZm9ybSA9IHRoaXM7XG4gICAgdmFyIGpzb24gPSBDb252ZXJ0Rm9ybVRvSlNPTihmb3JtKTtcbiAgICB2YXIgc25hbWUgPSB7IHNuYW1lOiAkKGZvcm0pLmF0dHIoJ2RhdGEtc25hbWUnKSB9O1xuICAgIHZhciBjbiA9ICQoJy5tYWlsaW5nbGlzdCcpLmZpbmQoJy5sZWdhbCcpO1xuICAgIHZhciBjb25zZW50X25vdGljZSA9IHsgY29uc2VudF9ub3RpY2U6IGNuLmh0bWwoKSB9O1xuICAgIHZhciBjdXJyZW50X3VybD0geyAnY3VycmVudF91cmwnOiB3aW5kb3cubG9jYXRpb24uaHJlZiB9O1xuICAgIC8vIGN1cnJlbnRfdXJsWydjdXJyZW50X3VybCddID0gd2luZG93LmxvY2F0aW9uLmhyZWZcbiAgICB2YXIgY29tcGxldGVKc29uID0gJC5leHRlbmQoIGpzb24sIHNuYW1lLCBjb25zZW50X25vdGljZSwgY3VycmVudF91cmwgKTtcbiAgICBkZWxldGUgY29tcGxldGVKc29uLm5vbl9odW1hbl9jaGVjaztcbiAgICB2YXIgc3RyaW5nSnNvbiA9IEpTT04uc3RyaW5naWZ5KGNvbXBsZXRlSnNvbik7XG4gICAgdmFyIGlzU3VtbWl0T25saW5lU2lnbnVwID0gJCh0aGlzKS5oYXNDbGFzcygnc3VtbWl0LW9ubGluZS1zaWdudXAtZm9ybScpO1xuXG4gICAgJC5hamF4KHtcbiAgICAgIHR5cGU6IFwiUE9TVFwiLFxuICAgICAgdXJsOiBcImh0dHBzOi8vd3d3LmFkb2JlLmNvbS9hcGkyL3N1YnNjcmliZV92MVwiLFxuICAgICAgZGF0YTogc3RyaW5nSnNvbixcbiAgICAgIGNvbnRlbnRUeXBlOlwiYXBwbGljYXRpb24vanNvbjsgY2hhcnNldD11dGYtOFwiLFxuICAgICAgZGF0YVR5cGU6IFwianNvblwiXG4gICAgfSkuZG9uZShmdW5jdGlvbihkYXRhKSB7XG5cdFx0XHRpZiggZm9ybS5oYXNBdHRyaWJ1dGUoJ2RhdGEtY2xlYXInKSApe1xuXHRcdFx0XHQkKGZvcm0pLmZpbmQoJy5mb3JtLWVsZW1lbnRzJykuaGlkZSgpO1xuICAgICAgfVxuICAgICAgaWYgKGlzU3VtbWl0T25saW5lU2lnbnVwKSB7XG4gICAgICAgICQoJy5tYWlsaW5nbGlzdC1zaWdudXAnKS5oaWRlKCk7XG4gICAgICAgICQoJy5tYWlsaW5nbGlzdC10aGFuay15b3UnKS5zaG93KCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAkKGZvcm0pLmZpbmQoJy5zdWNjZXNzLW1lc3NhZ2UnKS5zaG93KCk7XG4gICAgICB9XG5cdFx0XHQkKGZvcm0pLnJlc2V0KCk7XG4gICAgICBjb25zb2xlLmxvZyhkYXRhKTtcbiAgICB9KS5mYWlsKGZ1bmN0aW9uKGRhdGEpIHtcblx0XHRcdGlmKCBmb3JtLmhhc0F0dHJpYnV0ZSgnZGF0YS1jbGVhcicpICl7XG5cdFx0XHRcdCQoZm9ybSkuZmluZCgnLmZvcm0tZWxlbWVudHMnKS5oaWRlKCk7XG5cdFx0XHR9XG5cdFx0XHQkKGZvcm0pLmZpbmQoJy5mYWlsLW1lc3NhZ2UnKS5zaG93KCk7XG4gICAgICBjb25zb2xlLmxvZyhkYXRhKTtcbiAgICB9KTtcblxuICAgIHJldHVybiB0cnVlO1xufSk7XG5cbmZ1bmN0aW9uIENvbnZlcnRGb3JtVG9KU09OKGZvcm0pe1xuICB2YXIgYXJyYXkgPSBqUXVlcnkoZm9ybSkuc2VyaWFsaXplQXJyYXkoKTtcbiAgdmFyIGpzb24gPSB7fTtcblxuICBqUXVlcnkuZWFjaChhcnJheSwgZnVuY3Rpb24oKSB7XG4gICAganNvblt0aGlzLm5hbWVdID0gdGhpcy52YWx1ZSB8fCAnJztcbiAgfSk7XG5cbiAgcmV0dXJuIGpzb247XG59XG4iXX0=
