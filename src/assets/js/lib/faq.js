import $ from 'jquery';
'use strict';

$(document).ready(function () {

  var faqState = false;
  $('.faq-entries > ul > li > .accordion-title').click(function (e) {
    if ($('.accordion.dual-accordion > .accordion-item.is-active').length) {
      $('.dual-accordion').addClass('active-only');
      faqState = true;
    } else {
      if (faqState) {
        $('.dual-accordion').removeClass('active-only');
      }
      faqState = false;
    }
  });

  var faqSubState = false;
  $('.faq-entries .sub-accordion .accordion-title').click(function (e) {
    if ($('.accordion.sub-accordion > .accordion-item.is-active').length) {
      $(this).parent().parent().addClass('active-only');
      faqSubState = true;
    } else {
      if (faqSubState) {
        $(this).parent().parent().removeClass('active-only');
      }
      faqSubState = false;
    }
  });
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZhcS5qcyJdLCJuYW1lcyI6WyIkIiwiZG9jdW1lbnQiLCJyZWFkeSIsImZhcVN0YXRlIiwiY2xpY2siLCJlIiwibGVuZ3RoIiwiYWRkQ2xhc3MiLCJyZW1vdmVDbGFzcyIsImZhcVN1YlN0YXRlIiwicGFyZW50Il0sIm1hcHBpbmdzIjoiOztBQUFBQSxFQUFFQyxRQUFGLEVBQVlDLEtBQVosQ0FBa0IsWUFBVzs7QUFFM0IsTUFBSUMsV0FBVyxLQUFmO0FBQ0FILElBQUUsMkNBQUYsRUFBK0NJLEtBQS9DLENBQXFELFVBQVNDLENBQVQsRUFBWTtBQUMvRCxRQUFJTCxFQUFFLHVEQUFGLEVBQTJETSxNQUEvRCxFQUF1RTtBQUNyRU4sUUFBRSxpQkFBRixFQUFxQk8sUUFBckIsQ0FBOEIsYUFBOUI7QUFDQUosaUJBQVcsSUFBWDtBQUNELEtBSEQsTUFHTztBQUNMLFVBQUlBLFFBQUosRUFBYztBQUNaSCxVQUFFLGlCQUFGLEVBQXFCUSxXQUFyQixDQUFpQyxhQUFqQztBQUNEO0FBQ0RMLGlCQUFXLEtBQVg7QUFDRDtBQUNGLEdBVkQ7O0FBWUEsTUFBSU0sY0FBYyxLQUFsQjtBQUNBVCxJQUFFLDhDQUFGLEVBQWtESSxLQUFsRCxDQUF3RCxVQUFTQyxDQUFULEVBQVk7QUFDbEUsUUFBSUwsRUFBRSxzREFBRixFQUEwRE0sTUFBOUQsRUFBc0U7QUFDcEVOLFFBQUUsSUFBRixFQUFRVSxNQUFSLEdBQWlCQSxNQUFqQixHQUEwQkgsUUFBMUIsQ0FBbUMsYUFBbkM7QUFDQUUsb0JBQWMsSUFBZDtBQUNELEtBSEQsTUFHTztBQUNMLFVBQUlBLFdBQUosRUFBaUI7QUFDZlQsVUFBRSxJQUFGLEVBQVFVLE1BQVIsR0FBaUJBLE1BQWpCLEdBQTBCRixXQUExQixDQUFzQyxhQUF0QztBQUNEO0FBQ0RDLG9CQUFjLEtBQWQ7QUFDRDtBQUNGLEdBVkQ7QUFZRCxDQTVCRCIsImZpbGUiOiJmYXEuanMiLCJzb3VyY2VzQ29udGVudCI6WyIkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcblxuICB2YXIgZmFxU3RhdGUgPSBmYWxzZTtcbiAgJCgnLmZhcS1lbnRyaWVzID4gdWwgPiBsaSA+IC5hY2NvcmRpb24tdGl0bGUnKS5jbGljayhmdW5jdGlvbihlKSB7XG4gICAgaWYgKCQoJy5hY2NvcmRpb24uZHVhbC1hY2NvcmRpb24gPiAuYWNjb3JkaW9uLWl0ZW0uaXMtYWN0aXZlJykubGVuZ3RoKSB7XG4gICAgICAkKCcuZHVhbC1hY2NvcmRpb24nKS5hZGRDbGFzcygnYWN0aXZlLW9ubHknKTtcbiAgICAgIGZhcVN0YXRlID0gdHJ1ZTtcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKGZhcVN0YXRlKSB7XG4gICAgICAgICQoJy5kdWFsLWFjY29yZGlvbicpLnJlbW92ZUNsYXNzKCdhY3RpdmUtb25seScpO1xuICAgICAgfVxuICAgICAgZmFxU3RhdGUgPSBmYWxzZTtcbiAgICB9XG4gIH0pO1xuXG4gIHZhciBmYXFTdWJTdGF0ZSA9IGZhbHNlO1xuICAkKCcuZmFxLWVudHJpZXMgLnN1Yi1hY2NvcmRpb24gLmFjY29yZGlvbi10aXRsZScpLmNsaWNrKGZ1bmN0aW9uKGUpIHtcbiAgICBpZiAoJCgnLmFjY29yZGlvbi5zdWItYWNjb3JkaW9uID4gLmFjY29yZGlvbi1pdGVtLmlzLWFjdGl2ZScpLmxlbmd0aCkge1xuICAgICAgJCh0aGlzKS5wYXJlbnQoKS5wYXJlbnQoKS5hZGRDbGFzcygnYWN0aXZlLW9ubHknKTtcbiAgICAgIGZhcVN1YlN0YXRlID0gdHJ1ZTtcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKGZhcVN1YlN0YXRlKSB7XG4gICAgICAgICQodGhpcykucGFyZW50KCkucGFyZW50KCkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZS1vbmx5Jyk7XG4gICAgICB9XG4gICAgICBmYXFTdWJTdGF0ZSA9IGZhbHNlO1xuICAgIH1cbiAgfSk7XG5cbn0pO1xuIl19
