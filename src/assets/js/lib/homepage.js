import $ from 'jquery';
'use strict';

$(document).ready(function () {
  var showSmallSpeakersSet = Math.floor(Math.random() * Math.floor(2));
  if (showSmallSpeakersSet == 1) {
    $('.small-speakers-set-1').show();
    $('.small-brands-set-1').show();
  } else {
    $('.small-speakers-set-2').show();
    $('.small-brands-set-2').show();
  }
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhvbWVwYWdlLmpzIl0sIm5hbWVzIjpbIiQiLCJkb2N1bWVudCIsInJlYWR5Iiwic2hvd1NtYWxsU3BlYWtlcnNTZXQiLCJNYXRoIiwiZmxvb3IiLCJyYW5kb20iLCJzaG93Il0sIm1hcHBpbmdzIjoiOztBQUFBQSxFQUFFQyxRQUFGLEVBQVlDLEtBQVosQ0FBa0IsWUFBVTtBQUMxQixNQUFJQyx1QkFBdUJDLEtBQUtDLEtBQUwsQ0FBV0QsS0FBS0UsTUFBTCxLQUFnQkYsS0FBS0MsS0FBTCxDQUFXLENBQVgsQ0FBM0IsQ0FBM0I7QUFDQSxNQUFJRix3QkFBd0IsQ0FBNUIsRUFBK0I7QUFDN0JILE1BQUUsdUJBQUYsRUFBMkJPLElBQTNCO0FBQ0FQLE1BQUUscUJBQUYsRUFBeUJPLElBQXpCO0FBQ0QsR0FIRCxNQUdPO0FBQ0xQLE1BQUUsdUJBQUYsRUFBMkJPLElBQTNCO0FBQ0FQLE1BQUUscUJBQUYsRUFBeUJPLElBQXpCO0FBQ0Q7QUFDRixDQVREIiwiZmlsZSI6ImhvbWVwYWdlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKXtcbiAgdmFyIHNob3dTbWFsbFNwZWFrZXJzU2V0ID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogTWF0aC5mbG9vcigyKSk7XG4gIGlmIChzaG93U21hbGxTcGVha2Vyc1NldCA9PSAxKSB7XG4gICAgJCgnLnNtYWxsLXNwZWFrZXJzLXNldC0yJykuc2hvdygpO1xuICAgICQoJy5zbWFsbC1icmFuZHMtc2V0LTInKS5zaG93KCk7XG4gIH0gZWxzZSB7XG4gICAgJCgnLnNtYWxsLXNwZWFrZXJzLXNldC0xJykuc2hvdygpO1xuICAgICQoJy5zbWFsbC1icmFuZHMtc2V0LTEnKS5zaG93KCk7XG4gIH1cbn0pO1xuIl19
