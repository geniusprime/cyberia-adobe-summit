'use strict';

//$(document).ready(function(){
//	$('.single-item').slick({
//		dots: true,
//		arrows: true,
//		infinite: true,
//		speed: 300,
//		slidesToShow: 1,
//		centerMode: true,
//		variableWidth: true
//	});
//});


addOrRemoveSlides();

$(window).resize(function () {
	addOrRemoveSlides();
});

function addOrRemoveSlides() {
	if ($(window).width() < 1024) {
		initSlider();
	} else {
		destroySlider();
	}
}

function initSlider() {
	$('.slick-marquee').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		arrows: false
	});
}

function destroySlider() {
	$('.slick-marquee').slick('unslick');
}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1hcnF1ZWUtc2xpY2suanMiXSwibmFtZXMiOlsiYWRkT3JSZW1vdmVTbGlkZXMiLCIkIiwid2luZG93IiwicmVzaXplIiwid2lkdGgiLCJpbml0U2xpZGVyIiwiZGVzdHJveVNsaWRlciIsInNsaWNrIiwic2xpZGVzVG9TaG93Iiwic2xpZGVzVG9TY3JvbGwiLCJkb3RzIiwiYXJyb3dzIl0sIm1hcHBpbmdzIjoiOztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBQTs7QUFFQUMsRUFBR0MsTUFBSCxFQUFZQyxNQUFaLENBQW1CLFlBQVc7QUFDN0JIO0FBQ0EsQ0FGRDs7QUFJQSxTQUFTQSxpQkFBVCxHQUE2QjtBQUM1QixLQUFJQyxFQUFHQyxNQUFILEVBQVlFLEtBQVosS0FBc0IsSUFBMUIsRUFBK0I7QUFDOUJDO0FBQ0EsRUFGRCxNQUdLO0FBQ0pDO0FBQ0E7QUFDRDs7QUFFRCxTQUFTRCxVQUFULEdBQXFCO0FBQ3BCSixHQUFFLGdCQUFGLEVBQW9CTSxLQUFwQixDQUEwQjtBQUN6QkMsZ0JBQWMsQ0FEVztBQUV6QkMsa0JBQWdCLENBRlM7QUFHekJDLFFBQU0sSUFIbUI7QUFJekJDLFVBQVE7QUFKaUIsRUFBMUI7QUFNQTs7QUFFRCxTQUFTTCxhQUFULEdBQXlCO0FBQ3hCTCxHQUFFLGdCQUFGLEVBQW9CTSxLQUFwQixDQUEwQixTQUExQjtBQUNBIiwiZmlsZSI6Im1hcnF1ZWUtc2xpY2suanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCl7XG4vL1x0JCgnLnNpbmdsZS1pdGVtJykuc2xpY2soe1xuLy9cdFx0ZG90czogdHJ1ZSxcbi8vXHRcdGFycm93czogdHJ1ZSxcbi8vXHRcdGluZmluaXRlOiB0cnVlLFxuLy9cdFx0c3BlZWQ6IDMwMCxcbi8vXHRcdHNsaWRlc1RvU2hvdzogMSxcbi8vXHRcdGNlbnRlck1vZGU6IHRydWUsXG4vL1x0XHR2YXJpYWJsZVdpZHRoOiB0cnVlXG4vL1x0fSk7XG4vL30pO1xuXG5cbmFkZE9yUmVtb3ZlU2xpZGVzKCk7XG5cbiQoIHdpbmRvdyApLnJlc2l6ZShmdW5jdGlvbigpIHtcblx0YWRkT3JSZW1vdmVTbGlkZXMoKTtcbn0pO1xuXHRcbmZ1bmN0aW9uIGFkZE9yUmVtb3ZlU2xpZGVzKCkge1xuXHRpZiAoJCggd2luZG93ICkud2lkdGgoKSA8IDEwMjQpe1xuXHRcdGluaXRTbGlkZXIoKTtcblx0fVxuXHRlbHNlIHtcblx0XHRkZXN0cm95U2xpZGVyKCk7XG5cdH1cbn1cblxuZnVuY3Rpb24gaW5pdFNsaWRlcigpe1xuXHQkKCcuc2xpY2stbWFycXVlZScpLnNsaWNrKHtcblx0XHRzbGlkZXNUb1Nob3c6IDEsXG5cdFx0c2xpZGVzVG9TY3JvbGw6IDEsXG5cdFx0ZG90czogdHJ1ZSxcblx0XHRhcnJvd3M6IGZhbHNlXG5cdH0pO1xufVxuXG5mdW5jdGlvbiBkZXN0cm95U2xpZGVyKCkge1xuXHQkKCcuc2xpY2stbWFycXVlZScpLnNsaWNrKCd1bnNsaWNrJyk7XG59Il19
