'use strict';

$(document).ready(function () {
  $('.drop-item').click(function () {
    if ($(this).nextAll('.drop-section').length) {
      var $dropdown = $(this).nextAll('.drop-section:first');
      handleDropClick($(this), $dropdown);
    } else {
      var _$dropdown = $('#drop-section');
      handleDropClick($(this), _$dropdown);
    }
  });
  $('#drop-close-button').click(function () {
    close();
  });
});

function close() {
  $('.drop-section').removeClass('active');
  $('.drop-section').slideUp();
  $('.drop-item').removeClass('active');
  $('.drop-item').find('.arrow').fadeOut();
}

function handleDropClick($dropItem, $dropdown) {
  if ($dropItem.hasClass('active')) {
    $dropdown.removeClass('active');
    $dropdown.slideUp();
    $dropItem.removeClass('active');
    $dropItem.find('.arrow').fadeOut();
  } else {
    if ($('.drop-section.active').length) {
      $('.drop-item.active').find('.arrow').fadeOut();
      $('.drop-section.active').slideUp(function () {
        $('.active').removeClass('active');
        updateDropContent($dropItem, $dropdown);
        $dropdown.addClass('active');
        $dropdown.slideDown();
        $dropItem.find('.arrow').fadeIn();
        $dropItem.addClass('active');
      });
    } else {
      updateDropContent($dropItem, $dropdown);
      $dropdown.addClass('active');
      $dropdown.slideDown();
      $dropItem.addClass('active');
      $dropItem.find('.arrow').fadeIn();
    }
  }
}

function updateDropContent($dropItem, $dropdown) {
  if ($dropItem.hasClass('keynote-speaker-card')) {
    $dropdown.find('.keynote-logo img').attr({ "src": $dropItem.attr("data-speaker-profile") });
    $dropdown.find('.keynote-name').html($dropItem.attr("data-speaker-name"));
    $dropdown.find('.keynote-description').html($dropItem.attr("data-speaker-full-bio"));
  } else {
    $dropdown.find('.sponsor-logo img').attr({ "src": $dropItem.attr("data-sponsor-logo") });
    $dropdown.find('.sponsor-name').html($dropItem.attr("data-sponsor-name"));
    $dropdown.find('.sponsor-description').html($dropItem.attr("data-sponsor-description"));
    $dropdown.find('.sponsor-link').attr({ "href": $dropItem.attr("data-sponsor-url") });
    $dropdown.find('.sponsor-link').html($dropItem.attr("data-sponsor-url"));
  }
}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRyb3AtcmV2ZWFsLmpzIl0sIm5hbWVzIjpbIiQiLCJkb2N1bWVudCIsInJlYWR5IiwiY2xpY2siLCJuZXh0QWxsIiwibGVuZ3RoIiwiJGRyb3Bkb3duIiwiaGFuZGxlRHJvcENsaWNrIiwiY2xvc2UiLCJyZW1vdmVDbGFzcyIsInNsaWRlVXAiLCJmaW5kIiwiZmFkZU91dCIsIiRkcm9wSXRlbSIsImhhc0NsYXNzIiwidXBkYXRlRHJvcENvbnRlbnQiLCJhZGRDbGFzcyIsInNsaWRlRG93biIsImZhZGVJbiIsImF0dHIiLCJodG1sIl0sIm1hcHBpbmdzIjoiOztBQUFBQSxFQUFFQyxRQUFGLEVBQVlDLEtBQVosQ0FBa0IsWUFBVztBQUMzQkYsSUFBRSxZQUFGLEVBQWdCRyxLQUFoQixDQUFzQixZQUFVO0FBQzlCLFFBQUtILEVBQUUsSUFBRixFQUFRSSxPQUFSLENBQWdCLGVBQWhCLEVBQWlDQyxNQUF0QyxFQUE4QztBQUM1QyxVQUFJQyxZQUFZTixFQUFFLElBQUYsRUFBUUksT0FBUixDQUFnQixxQkFBaEIsQ0FBaEI7QUFDQUcsc0JBQWdCUCxFQUFFLElBQUYsQ0FBaEIsRUFBeUJNLFNBQXpCO0FBQ0QsS0FIRCxNQUlLO0FBQ0gsVUFBSUEsYUFBWU4sRUFBRSxlQUFGLENBQWhCO0FBQ0FPLHNCQUFnQlAsRUFBRSxJQUFGLENBQWhCLEVBQXlCTSxVQUF6QjtBQUNEO0FBQ0YsR0FURDtBQVVBTixJQUFFLG9CQUFGLEVBQXdCRyxLQUF4QixDQUE4QixZQUFVO0FBQ3RDSztBQUNELEdBRkQ7QUFHRCxDQWREOztBQWdCQSxTQUFTQSxLQUFULEdBQWdCO0FBQ2RSLElBQUUsZUFBRixFQUFtQlMsV0FBbkIsQ0FBK0IsUUFBL0I7QUFDQVQsSUFBRSxlQUFGLEVBQW1CVSxPQUFuQjtBQUNBVixJQUFFLFlBQUYsRUFBZ0JTLFdBQWhCLENBQTRCLFFBQTVCO0FBQ0FULElBQUUsWUFBRixFQUFnQlcsSUFBaEIsQ0FBcUIsUUFBckIsRUFBK0JDLE9BQS9CO0FBQ0Q7O0FBRUQsU0FBU0wsZUFBVCxDQUF5Qk0sU0FBekIsRUFBb0NQLFNBQXBDLEVBQStDO0FBQzdDLE1BQUlPLFVBQVVDLFFBQVYsQ0FBbUIsUUFBbkIsQ0FBSixFQUFrQztBQUNoQ1IsY0FBVUcsV0FBVixDQUFzQixRQUF0QjtBQUNBSCxjQUFVSSxPQUFWO0FBQ0FHLGNBQVVKLFdBQVYsQ0FBc0IsUUFBdEI7QUFDQUksY0FBVUYsSUFBVixDQUFlLFFBQWYsRUFBeUJDLE9BQXpCO0FBQ0QsR0FMRCxNQU1LO0FBQ0gsUUFBSVosRUFBRSxzQkFBRixFQUEwQkssTUFBOUIsRUFBcUM7QUFDbkNMLFFBQUUsbUJBQUYsRUFBdUJXLElBQXZCLENBQTRCLFFBQTVCLEVBQXNDQyxPQUF0QztBQUNBWixRQUFFLHNCQUFGLEVBQTBCVSxPQUExQixDQUFrQyxZQUFVO0FBQzFDVixVQUFFLFNBQUYsRUFBYVMsV0FBYixDQUF5QixRQUF6QjtBQUNBTSwwQkFBa0JGLFNBQWxCLEVBQTZCUCxTQUE3QjtBQUNBQSxrQkFBVVUsUUFBVixDQUFtQixRQUFuQjtBQUNBVixrQkFBVVcsU0FBVjtBQUNBSixrQkFBVUYsSUFBVixDQUFlLFFBQWYsRUFBeUJPLE1BQXpCO0FBQ0FMLGtCQUFVRyxRQUFWLENBQW1CLFFBQW5CO0FBQ0QsT0FQRDtBQVFELEtBVkQsTUFXSztBQUNIRCx3QkFBa0JGLFNBQWxCLEVBQTZCUCxTQUE3QjtBQUNBQSxnQkFBVVUsUUFBVixDQUFtQixRQUFuQjtBQUNBVixnQkFBVVcsU0FBVjtBQUNBSixnQkFBVUcsUUFBVixDQUFtQixRQUFuQjtBQUNBSCxnQkFBVUYsSUFBVixDQUFlLFFBQWYsRUFBeUJPLE1BQXpCO0FBQ0Q7QUFDRjtBQUNGOztBQUVELFNBQVNILGlCQUFULENBQTJCRixTQUEzQixFQUFzQ1AsU0FBdEMsRUFBaUQ7QUFDL0MsTUFBR08sVUFBVUMsUUFBVixDQUFtQixzQkFBbkIsQ0FBSCxFQUE4QztBQUM1Q1IsY0FBVUssSUFBVixDQUFlLG1CQUFmLEVBQW9DUSxJQUFwQyxDQUF5QyxFQUFDLE9BQVFOLFVBQVVNLElBQVYsQ0FBZSxzQkFBZixDQUFULEVBQXpDO0FBQ0FiLGNBQVVLLElBQVYsQ0FBZSxlQUFmLEVBQWdDUyxJQUFoQyxDQUFxQ1AsVUFBVU0sSUFBVixDQUFlLG1CQUFmLENBQXJDO0FBQ0FiLGNBQVVLLElBQVYsQ0FBZSxzQkFBZixFQUF1Q1MsSUFBdkMsQ0FBNENQLFVBQVVNLElBQVYsQ0FBZSx1QkFBZixDQUE1QztBQUNELEdBSkQsTUFLSztBQUNIYixjQUFVSyxJQUFWLENBQWUsbUJBQWYsRUFBb0NRLElBQXBDLENBQXlDLEVBQUMsT0FBUU4sVUFBVU0sSUFBVixDQUFlLG1CQUFmLENBQVQsRUFBekM7QUFDQWIsY0FBVUssSUFBVixDQUFlLGVBQWYsRUFBZ0NTLElBQWhDLENBQXFDUCxVQUFVTSxJQUFWLENBQWUsbUJBQWYsQ0FBckM7QUFDQWIsY0FBVUssSUFBVixDQUFlLHNCQUFmLEVBQXVDUyxJQUF2QyxDQUE0Q1AsVUFBVU0sSUFBVixDQUFlLDBCQUFmLENBQTVDO0FBQ0FiLGNBQVVLLElBQVYsQ0FBZSxlQUFmLEVBQWdDUSxJQUFoQyxDQUFxQyxFQUFDLFFBQVNOLFVBQVVNLElBQVYsQ0FBZSxrQkFBZixDQUFWLEVBQXJDO0FBQ0FiLGNBQVVLLElBQVYsQ0FBZSxlQUFmLEVBQWdDUyxJQUFoQyxDQUFxQ1AsVUFBVU0sSUFBVixDQUFlLGtCQUFmLENBQXJDO0FBQ0Q7QUFDRiIsImZpbGUiOiJkcm9wLXJldmVhbC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xuICAkKCcuZHJvcC1pdGVtJykuY2xpY2soZnVuY3Rpb24oKXtcbiAgICBpZiAoICQodGhpcykubmV4dEFsbCgnLmRyb3Atc2VjdGlvbicpLmxlbmd0aCApe1xuICAgICAgbGV0ICRkcm9wZG93biA9ICQodGhpcykubmV4dEFsbCgnLmRyb3Atc2VjdGlvbjpmaXJzdCcpO1xuICAgICAgaGFuZGxlRHJvcENsaWNrKCQodGhpcyksICRkcm9wZG93bik7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgbGV0ICRkcm9wZG93biA9ICQoJyNkcm9wLXNlY3Rpb24nKTtcbiAgICAgIGhhbmRsZURyb3BDbGljaygkKHRoaXMpLCAkZHJvcGRvd24pO1xuICAgIH1cbiAgfSk7XG4gICQoJyNkcm9wLWNsb3NlLWJ1dHRvbicpLmNsaWNrKGZ1bmN0aW9uKCl7XG4gICAgY2xvc2UoKTtcbiAgfSk7XG59KTtcblxuZnVuY3Rpb24gY2xvc2UoKXtcbiAgJCgnLmRyb3Atc2VjdGlvbicpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgJCgnLmRyb3Atc2VjdGlvbicpLnNsaWRlVXAoKTtcbiAgJCgnLmRyb3AtaXRlbScpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgJCgnLmRyb3AtaXRlbScpLmZpbmQoJy5hcnJvdycpLmZhZGVPdXQoKTtcbn1cblxuZnVuY3Rpb24gaGFuZGxlRHJvcENsaWNrKCRkcm9wSXRlbSwgJGRyb3Bkb3duKSB7XG4gIGlmICgkZHJvcEl0ZW0uaGFzQ2xhc3MoJ2FjdGl2ZScpKSB7XG4gICAgJGRyb3Bkb3duLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAkZHJvcGRvd24uc2xpZGVVcCgpO1xuICAgICRkcm9wSXRlbS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgJGRyb3BJdGVtLmZpbmQoJy5hcnJvdycpLmZhZGVPdXQoKTtcbiAgfVxuICBlbHNlIHtcbiAgICBpZiAoJCgnLmRyb3Atc2VjdGlvbi5hY3RpdmUnKS5sZW5ndGgpe1xuICAgICAgJCgnLmRyb3AtaXRlbS5hY3RpdmUnKS5maW5kKCcuYXJyb3cnKS5mYWRlT3V0KCk7XG4gICAgICAkKCcuZHJvcC1zZWN0aW9uLmFjdGl2ZScpLnNsaWRlVXAoZnVuY3Rpb24oKXtcbiAgICAgICAgJCgnLmFjdGl2ZScpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgdXBkYXRlRHJvcENvbnRlbnQoJGRyb3BJdGVtLCAkZHJvcGRvd24pO1xuICAgICAgICAkZHJvcGRvd24uYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAkZHJvcGRvd24uc2xpZGVEb3duKCk7XG4gICAgICAgICRkcm9wSXRlbS5maW5kKCcuYXJyb3cnKS5mYWRlSW4oKTtcbiAgICAgICAgJGRyb3BJdGVtLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIHVwZGF0ZURyb3BDb250ZW50KCRkcm9wSXRlbSwgJGRyb3Bkb3duKTtcbiAgICAgICRkcm9wZG93bi5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgICAkZHJvcGRvd24uc2xpZGVEb3duKCk7XG4gICAgICAkZHJvcEl0ZW0uYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgJGRyb3BJdGVtLmZpbmQoJy5hcnJvdycpLmZhZGVJbigpO1xuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiB1cGRhdGVEcm9wQ29udGVudCgkZHJvcEl0ZW0sICRkcm9wZG93bikge1xuICBpZigkZHJvcEl0ZW0uaGFzQ2xhc3MoJ2tleW5vdGUtc3BlYWtlci1jYXJkJykpe1xuICAgICRkcm9wZG93bi5maW5kKCcua2V5bm90ZS1sb2dvIGltZycpLmF0dHIoe1wic3JjXCIgOiAkZHJvcEl0ZW0uYXR0cihcImRhdGEtc3BlYWtlci1wcm9maWxlXCIpfSk7XG4gICAgJGRyb3Bkb3duLmZpbmQoJy5rZXlub3RlLW5hbWUnKS5odG1sKCRkcm9wSXRlbS5hdHRyKFwiZGF0YS1zcGVha2VyLW5hbWVcIikpO1xuICAgICRkcm9wZG93bi5maW5kKCcua2V5bm90ZS1kZXNjcmlwdGlvbicpLmh0bWwoJGRyb3BJdGVtLmF0dHIoXCJkYXRhLXNwZWFrZXItZnVsbC1iaW9cIikpO1xuICB9XG4gIGVsc2Uge1xuICAgICRkcm9wZG93bi5maW5kKCcuc3BvbnNvci1sb2dvIGltZycpLmF0dHIoe1wic3JjXCIgOiAkZHJvcEl0ZW0uYXR0cihcImRhdGEtc3BvbnNvci1sb2dvXCIpfSk7XG4gICAgJGRyb3Bkb3duLmZpbmQoJy5zcG9uc29yLW5hbWUnKS5odG1sKCRkcm9wSXRlbS5hdHRyKFwiZGF0YS1zcG9uc29yLW5hbWVcIikpO1xuICAgICRkcm9wZG93bi5maW5kKCcuc3BvbnNvci1kZXNjcmlwdGlvbicpLmh0bWwoJGRyb3BJdGVtLmF0dHIoXCJkYXRhLXNwb25zb3ItZGVzY3JpcHRpb25cIikpO1xuICAgICRkcm9wZG93bi5maW5kKCcuc3BvbnNvci1saW5rJykuYXR0cih7XCJocmVmXCIgOiAkZHJvcEl0ZW0uYXR0cihcImRhdGEtc3BvbnNvci11cmxcIil9KTtcbiAgICAkZHJvcGRvd24uZmluZCgnLnNwb25zb3ItbGluaycpLmh0bWwoJGRyb3BJdGVtLmF0dHIoXCJkYXRhLXNwb25zb3ItdXJsXCIpKTtcbiAgfVxufSJdfQ==
